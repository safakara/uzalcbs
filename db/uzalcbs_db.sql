-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 18 Ağu 2017, 20:13:33
-- Sunucu sürümü: 10.1.25-MariaDB
-- PHP Sürümü: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `uzalcbs_db`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `analiz_detay`
--

CREATE TABLE `analiz_detay` (
  `id` int(8) NOT NULL,
  `istasyon_id` int(8) NOT NULL,
  `analiz_tipi_id` int(8) NOT NULL,
  `su_max_seviye` decimal(16,8) DEFAULT NULL,
  `su_min_seviye` decimal(16,8) DEFAULT NULL,
  `su_max_sicaklik` decimal(16,8) DEFAULT NULL,
  `su_min_sicaklik` decimal(16,8) DEFAULT NULL,
  `su_max_tahliye` decimal(16,8) DEFAULT NULL,
  `su_min_tahliye` decimal(16,8) DEFAULT NULL,
  `numune_dusey` decimal(16,8) DEFAULT NULL,
  `su_derinligi` decimal(16,8) DEFAULT NULL,
  `esel_seviye` decimal(16,8) DEFAULT NULL,
  `olcek_seri_no` varchar(20) DEFAULT NULL,
  `sediment_yuzdesi` decimal(16,8) DEFAULT NULL,
  `sediment_miktari` decimal(16,8) DEFAULT NULL,
  `kil` decimal(16,8) DEFAULT NULL,
  `silt` decimal(16,8) DEFAULT NULL,
  `kum` decimal(16,8) DEFAULT NULL,
  `tekstur` decimal(16,8) DEFAULT NULL,
  `ph` decimal(16,8) DEFAULT NULL,
  `bod` decimal(16,8) DEFAULT NULL COMMENT 'çözünmüş oksijen miktarı',
  `kimyasal_madde_id` int(8) DEFAULT NULL,
  `kimyasal_madde_deger` decimal(16,8) DEFAULT NULL,
  `okuma_zamani` datetime NOT NULL,
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `analiz_tipi`
--

CREATE TABLE `analiz_tipi` (
  `id` int(8) NOT NULL,
  `isim` varchar(50) NOT NULL,
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `dinamik_rapor`
--

CREATE TABLE `dinamik_rapor` (
  `id` int(8) NOT NULL,
  `aciklama` varchar(255) NOT NULL,
  `sql_query` text NOT NULL,
  `form_inputs` text,
  `js` text,
  `query_auto_exec` bit(1) NOT NULL,
  `js_paging` bit(1) NOT NULL,
  `js_page_item_count` int(8) DEFAULT NULL,
  `durum` bit(1) NOT NULL,
  `yetki` varchar(100) DEFAULT NULL,
  `kullanici` varchar(40) DEFAULT NULL,
  `tarih` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `dinamik_rapor`
--

INSERT INTO `dinamik_rapor` (`id`, `aciklama`, `sql_query`, `form_inputs`, `js`, `query_auto_exec`, `js_paging`, `js_page_item_count`, `durum`, `yetki`, `kullanici`, `tarih`) VALUES
(1, 'test', 'select * from kullanici', '[]', ' \r\n	$(function(){\r\n		/*\r\n		 * JS Code here..\r\n		 */\r\n		 \r\n	});\r\n', b'1', b'1', 11, b'1', '1', 'Ahmet Safa Kara', '2017-08-18 20:50:38'),
(2, 'test2', 'select * from kullanici where kullanici_grup_id = :kullaniciGrupId', '[{\"_name\":\"kullaniciGrupId\",\"_type\":\"selectApi\",\"_class\":\"required\",\"_width\":\"6\",\"_title\":\"Kullanıcı Grubu\",\"_value\":\"\",\"_api\":\"kullanici-grup\",\"_apiKey\":\"id\",\"_apiVal\":\"isim\"}]', ' \r\n	$(function(){\r\n		/*\r\n		 * JS Code here..\r\n		 */\r\n		 \r\n	});\r\n', b'0', b'0', NULL, b'1', '1', 'Ahmet Safa Kara', '2017-08-18 21:09:37');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `istasyon`
--

CREATE TABLE `istasyon` (
  `id` int(8) NOT NULL,
  `isim` varchar(100) NOT NULL,
  `aygit_tipi` varchar(15) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `port` int(5) DEFAULT '80',
  `bolge` tinyint(1) DEFAULT NULL,
  `enlem` decimal(20,12) DEFAULT NULL,
  `boylam` decimal(20,12) DEFAULT NULL,
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kimyasal_madde`
--

CREATE TABLE `kimyasal_madde` (
  `id` int(8) NOT NULL,
  `isim` varchar(50) NOT NULL,
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanici`
--

CREATE TABLE `kullanici` (
  `id` int(8) NOT NULL,
  `kullanici_grup_id` int(8) DEFAULT NULL,
  `mail` varchar(50) NOT NULL,
  `sifre` varchar(32) NOT NULL,
  `isim` varchar(50) NOT NULL,
  `telefon` varchar(20) DEFAULT NULL,
  `adres` varchar(100) DEFAULT NULL,
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `kullanici`
--

INSERT INTO `kullanici` (`id`, `kullanici_grup_id`, `mail`, `sifre`, `isim`, `telefon`, `adres`, `olusturma_tarihi`) VALUES
(1, 1, 'safakara01@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', 'Ahmet Safa Kara', NULL, NULL, '2017-08-15 13:41:11');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanici_grup`
--

CREATE TABLE `kullanici_grup` (
  `id` int(8) NOT NULL,
  `isim` varchar(50) NOT NULL,
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `kullanici_grup`
--

INSERT INTO `kullanici_grup` (`id`, `isim`, `olusturma_tarihi`) VALUES
(1, 'ADMIN', '2017-08-15 13:40:35');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sayfa`
--

CREATE TABLE `sayfa` (
  `id` int(8) NOT NULL,
  `adi` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sayfa_yetkileri`
--

CREATE TABLE `sayfa_yetkileri` (
  `id` int(8) NOT NULL,
  `sayfa_id` int(8) NOT NULL,
  `kullanici_grup_id` int(8) NOT NULL,
  `yetki_tipi_id` int(8) NOT NULL,
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yetki_tipi`
--

CREATE TABLE `yetki_tipi` (
  `id` int(8) NOT NULL,
  `isim` varchar(50) NOT NULL,
  `SON_EK` varchar(50) NOT NULL COMMENT 'Security layer''da kullanılacak anahtar kelimedir',
  `olusturma_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `yetki_tipi`
--

INSERT INTO `yetki_tipi` (`id`, `isim`, `SON_EK`, `olusturma_tarihi`) VALUES
(1, 'Okuma', 'OKUMA', '2017-08-14 11:52:08'),
(2, 'Yazma', 'YAZMA', '2017-08-14 10:31:10'),
(3, 'Silme', 'SILME', '2017-08-14 10:31:25');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `analiz_detay`
--
ALTER TABLE `analiz_detay`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_AD_ANALIZ_TIPI` (`analiz_tipi_id`),
  ADD KEY `FK_AD_KIMYASAL_MADDE` (`kimyasal_madde_id`),
  ADD KEY `FK_AD_ISTASYON` (`istasyon_id`);

--
-- Tablo için indeksler `analiz_tipi`
--
ALTER TABLE `analiz_tipi`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `dinamik_rapor`
--
ALTER TABLE `dinamik_rapor`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `istasyon`
--
ALTER TABLE `istasyon`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `kimyasal_madde`
--
ALTER TABLE `kimyasal_madde`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `kullanici`
--
ALTER TABLE `kullanici`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`),
  ADD KEY `FK_K_KULLANICI_GRUP` (`kullanici_grup_id`);

--
-- Tablo için indeksler `kullanici_grup`
--
ALTER TABLE `kullanici_grup`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `sayfa`
--
ALTER TABLE `sayfa`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `sayfa_yetkileri`
--
ALTER TABLE `sayfa_yetkileri`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_SY_YETKI_TIPI` (`yetki_tipi_id`),
  ADD KEY `FK_SY_KULLANICI_GRUP` (`kullanici_grup_id`),
  ADD KEY `FK_SY_SAYFA` (`sayfa_id`);

--
-- Tablo için indeksler `yetki_tipi`
--
ALTER TABLE `yetki_tipi`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `analiz_detay`
--
ALTER TABLE `analiz_detay`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `analiz_tipi`
--
ALTER TABLE `analiz_tipi`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `dinamik_rapor`
--
ALTER TABLE `dinamik_rapor`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Tablo için AUTO_INCREMENT değeri `istasyon`
--
ALTER TABLE `istasyon`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `kimyasal_madde`
--
ALTER TABLE `kimyasal_madde`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `kullanici`
--
ALTER TABLE `kullanici`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Tablo için AUTO_INCREMENT değeri `kullanici_grup`
--
ALTER TABLE `kullanici_grup`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Tablo için AUTO_INCREMENT değeri `sayfa`
--
ALTER TABLE `sayfa`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `sayfa_yetkileri`
--
ALTER TABLE `sayfa_yetkileri`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `yetki_tipi`
--
ALTER TABLE `yetki_tipi`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `analiz_detay`
--
ALTER TABLE `analiz_detay`
  ADD CONSTRAINT `FK_AD_ANALIZ_TIPI` FOREIGN KEY (`analiz_tipi_id`) REFERENCES `analiz_tipi` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_AD_ISTASYON` FOREIGN KEY (`istasyon_id`) REFERENCES `istasyon` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_AD_KIMYASAL_MADDE` FOREIGN KEY (`kimyasal_madde_id`) REFERENCES `kimyasal_madde` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `kullanici`
--
ALTER TABLE `kullanici`
  ADD CONSTRAINT `FK_K_KULLANICI_GRUP` FOREIGN KEY (`kullanici_grup_id`) REFERENCES `kullanici_grup` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `sayfa_yetkileri`
--
ALTER TABLE `sayfa_yetkileri`
  ADD CONSTRAINT `FK_SY_KULLANICI_GRUP` FOREIGN KEY (`kullanici_grup_id`) REFERENCES `kullanici_grup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_SY_SAYFA` FOREIGN KEY (`sayfa_id`) REFERENCES `sayfa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_SY_YETKI_TIPI` FOREIGN KEY (`yetki_tipi_id`) REFERENCES `yetki_tipi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
