<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page session="true" contentType="text/html; charset=UTF-8"%>
<%@include file="../template/header.jsp"%>

<title>UzalCBS | Kullanıcı Grup İşlemleri</title>
<%@include file="../template/menu.jsp"%>

<div class="menu-selector" data-item="kullaniciGrup"></div>
<div class="content-wrapper" style="min-height: 1066px;">

	<section class="content">
		
		<sec:authorize access="hasAnyRole('ROLE_ADMIN','KULLANICI_GRUP_YAZMA')">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Kullanıcı Grup İşlemleri</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>	</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<c:if test="${not empty msg}">
									<div class="col-md-12">
										<div class="callout callout-success">
											<h4><i class="fa fa-check-circle"></i> Başarılı</h4>
											<p>${msg}</p>
										</div>
									</div>
								</c:if>

								<c:if test="${empty kullaniciGrup.id}">
									<c:set var="formURL" scope="session" value="${pageContext.request.contextPath}/admin/kullanici-grup" />
								</c:if>
								<c:if test="${not empty kullaniciGrup.id}">
									<c:set var="formURL" scope="session" value="${pageContext.request.contextPath}/admin/kullanici-grup/edit/${kullaniciGrup.id}" />
								</c:if>

								<form:form method="POST" commandName="kullaniciGrup" action="${formURL}">
									<div class="col-md-4">
										<div class="form-group">
											<label for="tip">ID</label>
											<form:input path="id" id="id" readonly="true" cssClass="form-control" />
										</div>
									</div>

									<div class="col-md-8">
										<div class="form-group">
											<label for="isim">İsim</label>
											<form:input path="isim" id="isim" cssClass="required form-control" />
										</div>
									</div>

									<div class="col-md-4 col-md-offset-8">
										<div class="pull-right">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
											<a href="${pageContext.request.contextPath}/admin/kullanici-grup" class="btn  btn-default btn-flat">
												<i class="fa fa-repeat"></i> Temizle
											</a>

											<c:if test="${empty kullaniciGrup.id}">
												<button type="submit" class="btn bg-olive btn-flat margin">
													<i class="fa fa-save"></i> Kaydet
												</button>
											</c:if>
											<c:if test="${not empty kullaniciGrup.id}">
												<button type="submit" class="btn bg-orange btn-flat margin">
													<i class="fa fa-save"></i> Güncelle
												</button>
											</c:if>

										</div>
									</div>

								</form:form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		</sec:authorize>



		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Kullanıcı Grup Listesi</h3>
						<div class="box-tools pull-right">
							<div class="btn-group">
								<button title="Raporlama" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-print"></i>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><i class="fa fa-file-excel-o"></i> XLS</a></li>
								</ul>
							</div>
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<table id="dataTable" class="table table-bordered table-striped dataTableClass">
									<thead>
										<tr>
											<th>ID</th>
											<th>İsim</th>
											<th>Oluşturma Tarihi</th>
											<sec:authorize access="hasAnyRole('ROLE_ADMIN','KULLANICI_GRUP_SILME')"><th>Silme</th></sec:authorize>
											<sec:authorize access="hasAnyRole('ROLE_ADMIN','KULLANICI_GRUP_YAZMA')"><th>Düzeltme</th></sec:authorize>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="tmp" items="${kullaniciGrupList}">
											<tr>
												<td>${tmp.id}</td>
												<td>${tmp.isim}</td>
												<td><fmt:formatDate value="${tmp.olusturmaTarihi}" pattern="dd.MM.yyyy HH:mm" /></td>
												<sec:authorize access="hasAnyRole('ROLE_ADMIN','KULLANICI_GRUP_SILME')">
													<td width="30">
														<a href="${pageContext.request.contextPath}/admin/kullanici-grup/delete/${tmp.id}"	class="deleteRow" title="Sil">
															<i class="fa fa-trash-o"></i>
														</a>
													</td>
												</sec:authorize>
												
												<sec:authorize access="hasAnyRole('ROLE_ADMIN','KULLANICI_GRUP_YAZMA')">
													<td width="30">
														<a href="${pageContext.request.contextPath}/admin/kullanici-grup/edit/${tmp.id}" title="Düzenle">
															<i class="fa fa-edit"></i>
														</a>
													</td>
												</sec:authorize>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


<%@include file="../template/adminFooter.jsp"%>

