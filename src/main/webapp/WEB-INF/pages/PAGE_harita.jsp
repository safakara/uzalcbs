<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page session="true" contentType="text/html; charset=UTF-8"%>
<%@include file="../template/header.jsp"%>

<title>UzalCBS | Harita</title>
<%@include file="../template/menu.jsp"%>

<div class="menu-selector" data-item="harita"></div>
<div class="content-wrapper" style="min-height: 1066px;">

	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Harita</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>	</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div id="mapView" style="width:100%; height:700px"></div>
							    <script>
							      var map;
							      function initMap() {
							        map = new google.maps.Map(document.getElementById('mapView'), {
							          center: {lat: 36.9946179, lng: 35.175422},
							          zoom: 8
							        });
							      }
							    </script>
							    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9cPUTgV3RTHmzSmx7i07EiS_Dy2M5bfU&callback=initMap" async defer></script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>
</div>


<%@include file="../template/adminFooter.jsp"%>

