<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page session="true" contentType="text/html; charset=UTF-8"%>
<%@include file="../template/header.jsp"%>

<title>UzalCBS | Web Roles İşlemleri</title>
<%@include file="../template/menu.jsp"%>

<div class="menu-selector" data-item="webRoles"></div>
<div class="content-wrapper" style="min-height: 1066px;">

	<section class="content">

		<c:if test="${not empty msg}">
			<div class="row">
				<div class="col-md-12">
					<div class="callout callout-success">
						<h4>
							<i class="fa fa-check-circle"></i> Başarılı
						</h4>
						<p>${msg}</p>
					</div>
				</div>
			</div>
		</c:if>


		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Web Roles Listesi</h3> 
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_WEB_ROLES_YAZMA')">							
								<a href="web-roles/add" class="btn bg-olive btn-flat margin pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-save"></i> Ekle</a>
					</sec:authorize>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<table id="dataTable" class="table table-bordered table-striped dataTableClass">
									<thead>
										<tr>
											<th>ID</th>
											<th>Adı</th>
											<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_WEB_ROLES_SILME')"><th>Silme</th></sec:authorize>											
											<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_WEB_ROLES_YAZMA')"><th>Düzeltme</th></sec:authorize>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="tmpWebRolesList" items="${webRolesList}">
											<tr>
												<td>${tmpWebRolesList.id}</td>
												<td>${tmpWebRolesList.name}</td>
												<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_WEB_ROLES_SILME')">
													<td width="30"><a href="${pageContext.request.contextPath}/admin/web-roles/delete/${tmpWebRolesList.id}" class="deleteRow" title="Sil"><i class="fa fa-trash-o"></i></a></td>
												</sec:authorize>
												<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_WEB_ROLES_YAZMA')">
													<td width="30"><a href="${pageContext.request.contextPath}/admin/web-roles/edit/${tmpWebRolesList.id}" data-toggle="modal" data-target="#myModal" title="Güncelle"><i class="fa fa-edit"></i></a></td>
												</sec:authorize>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<%@include file="../template/adminFooter.jsp"%>
<%@include file="../template/myModal.jsp"%>
