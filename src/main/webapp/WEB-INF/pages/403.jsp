<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true" contentType="text/html; charset=UTF-8"%>
<%@include file="../template/header.jsp" %>

<title>UzalCBS | 403</title>
<%@include file="../template/navbar.jsp" %>

      <section style="min-height:400px; background-color:white" class="content">

          <div  class="error-page">
            <h2 class="headline text-red">403</h2>
            <div style="padding-top:20px" class="error-content">
              <h3><i class="fa fa-warning text-red"></i> Oops!</h3>
              <p>
                Bu alana erişmek için yetkiniz bulunmamaktadır. Lütfen yetkililerle iletişime geçiniz.
              </p>
            </div>
          </div>
        </section></div>
        
<%@include file="../template/adminFooter.jsp" %>
 <script type="text/javascript">
	$(function(){
		$(".content").height($(window).height()-140);
	});
</script>