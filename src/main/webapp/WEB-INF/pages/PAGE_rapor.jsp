<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page session="true" contentType="text/html; charset=UTF-8"%>
<%@include file="../template/header.jsp"%>

<title>UzalCBS | ${dinamikRapor.aciklama}</title>
<%@include file="../template/menu.jsp"%>

<div class="menu-selector" data-item="r${dinamikRapor.id}"></div>
<div class="content-wrapper" style="min-height: 1066px;">

	<section class="content">

		<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_RAPORLAMA_OKUMA')">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-success">
						<div class="box-header with-border">
							<h3 class="box-title">${dinamikRapor.aciklama}</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<form:form method="POST" action="${pageContext.request.contextPath}/admin/rapor/${dinamikRapor.id}">
									

									${inputForm}

									<div class="col-md-4 col-md-offset-8">
											<div class="pull-right">
												<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

												<button type="submit" class="btn bg-olive btn-flat margin">
													<i class="fa fa-search"></i> Sorgula
												</button>

											</div>
										</div>

									</form:form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</sec:authorize>



		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">${dinamikRapor.aciklama}Listesi</h3>
						<div class="box-tools pull-right">
							<div class="btn-group">
								<button title="Raporlama" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-print"></i>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#" onclick="tableToExcel('reportTable', 'Page 1')"><i class="fa fa-file-excel-o"></i> XLS</a></li>
									<li><a href="#" onclick="setupDownloadLink(this, 'rawData')" download="data.txt"><i class="fa fa-file-text-o"></i> TXT</a></li>
								</ul>
							</div>
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">${table}</div>
						</div>

						<div id="rawData" style="display: none;">${rawData}</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


<%@include file="../template/adminFooter.jsp"%>


<c:if test="${dinamikRapor.jsPaging}">
	<script type="text/javascript">
		$(function(){
			$('#reportTable').dataTable({
					"iDisplayLength" : 	
						<c:if test="${(dinamikRapor.jsPageItemCount != null)}">${dinamikRapor.jsPageItemCount}</c:if>
						<c:if test="${(dinamikRapor.jsPageItemCount == null)}">10</c:if>,
						
					"aLengthMenu": [[
										<c:if test="${(dinamikRapor.jsPageItemCount != null)}">${dinamikRapor.jsPageItemCount}</c:if>
										<c:if test="${(dinamikRapor.jsPageItemCount == null)}">10</c:if>
					                 	, -1
					                 ], 
					                 [
										<c:if test="${(dinamikRapor.jsPageItemCount != null)}">${dinamikRapor.jsPageItemCount}</c:if>
										<c:if test="${(dinamikRapor.jsPageItemCount == null)}">10</c:if>
					                 	, "Hepsi"
					                 ]
									]
			});
		});
	
	</script>
</c:if>

<script type="text/javascript">
	${dinamikRapor.js}
</script>


<script type="text/javascript">

function setupDownloadLink(link, divId) {
	var rawData = document.getElementById(divId).innerHTML;
    link.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(rawData);
  };

</script>
