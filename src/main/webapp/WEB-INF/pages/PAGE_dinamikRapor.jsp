<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page session="true" contentType="text/html; charset=UTF-8"%>
<%@include file="../template/header.jsp"%>

<title>UzalCBS | Dinamik Rapor İşlemleri</title>
<link href="<c:url value="/assets/plugins/codemirror-5.10/addon/hint/show-hint.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/assets/plugins/codemirror-5.10/lib/codemirror.css" />" rel="stylesheet" type="text/css" />
<%@include file="../template/menu.jsp"%>

<div class="menu-selector" data-item="dinamikRaporIslemleri"></div>
<div class="content-wrapper" style="min-height: 1066px;">

	<section class="content">
		
		<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_DINAMIK_RAPOR_OLUSTUR_YAZMA')">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Dinamik Rapor İşlemleri</h3>
						<div class="box-tools pull-right">
							<a href="${pageContext.request.contextPath}/admin/dinamik-rapor/olustur" class="btn btn-sm btn-flat bg-navy"><i class="fa fa-repeat"></i> Yeni</a>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<c:if test="${not empty msg}">
									<div class="col-md-12">
										<div class="callout callout-success">
											<h4>
												<i class="fa fa-check-circle"></i> Başarılı
											</h4>
											<p>${msg}</p>
										</div>
									</div>
								</c:if>

								<c:if test="${empty dinamikRapor.id}">
									<c:set var="formURL" scope="session"
										value="${pageContext.request.contextPath}/admin/dinamik-rapor/olustur" />
								</c:if>
								<c:if test="${not empty dinamikRapor.id}">
									<c:set var="formURL" scope="session"
										value="${pageContext.request.contextPath}/admin/dinamik-rapor/guncelle/${dinamikRapor.id}" />
								</c:if>

								<form:form cssClass="dinamikRaporForm" method="POST" commandName="dinamikRapor" action="${formURL}">
									
									<form:textarea path="sqlQuery" cssClass="sqlQuery" cssStyle="display:none"/>
									<form:textarea path="formInputs" cssClass="formInputs" cssStyle="display:none"/>
									<form:input path="yetki" cssClass="yetki" cssStyle="display:none"/>
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									
									
									<div class="col-md-6">
										<div class="form-group">
											<label for="aciklama">Açıklama</label>
											<form:input path="aciklama" id="aciklama" cssClass="form-control" style="height:42px"/>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label for="aciklama">Yetki</label> 
											<select data-placeholder=" " class="form-control autoComplate yetkiSelect" 
													data-selected="${dinamikRapor.yetki}" data-first-empty="true" 
													data-url="kullanici-grup" data-value="id" data-text="isim"
													required multiple style="height:45px">

											</select>
										</div>
									</div>
									
									<div class="col-md-3">
										<form:checkbox path="queryAutoExec" label=" Sayfa açılışında sorguyu çalıştır"/>
									</div>
									
									<div class="col-md-3">
										<form:checkbox path="durum" label=" Aktif"/>
									</div>
									
									<div class="col-md-3">
										<form:checkbox path="jsPaging" cssClass="jsPaging" label=" JS Sayfalama"/>
									</div>
									
									<div class="col-md-3">
										<div class="col-md-6">
											<div class="pull-right">
												<label for="jsPageItemCount">JS Kayıt Sayısı</label>
											</div>
										</div>
										<div class="col-md-6" style="padding-right:0">
											<form:input path="jsPageItemCount" id="jsPageItemCount" cssClass="form-control number jsPageItemCount" readonly="true" />
										</div>
									</div>
									
									<hr>
									<div class="col-md-12" style="margin-bottom:40px"></div>
									 
									 	
									 
									<div data-example-id="togglable-tabs">
									   <ul id="myTabs" class="nav nav-tabs" role="tablist">
									      <li role="presentation" class="active"><a href="#sql" id="home-tab" role="tab" data-toggle="tab" aria-controls="sql" aria-expanded="true">SQL</a></li>
									      <li role="presentation" class=""><a href="#js" role="tab" id="profile-tab" data-toggle="tab" aria-controls="js" aria-expanded="false">JavaScript</a></li>
									   </ul>
									   
									   <div id="myTabContent" class="tab-content tab-detail-style">
									      <div role="tabpanel" class="tab-pane fade active in" id="sql" aria-labelledby="home-tab" style="padding:10px">
									      		<div class="col-md-11">
													<label for="tmpSqlQuery">SQL Query</label>
													<textarea id="tmpSqlQuery">${dinamikRapor.sqlQuery}</textarea>
												</div>
									
												<div class="col-md-1" style="padding-top:20px">
													<button type="button" class="btn btn-block bg-olive btn-flat btnCozumle">
														<i class="fa fa-play"></i> Çözümle
													</button>
													
													<button type="button" class="btn btn-block bg-maroon btn-flat btnTemizle">
														<i class="fa  fa-remove"></i> Temizle
													</button>
												</div>

												<div class="row">
													<div class="col-md-12" style="margin-top:40px">
														<table class="table parametreTable" style="display:none;">
															<tr>
																<th>Name</th>
																<th>Title</th>
																<th>Type</th>
																<th>Width</th>
																<th>Class</th>
																<th>Value</th>
																<th>Api</th>
																<th>ApiKey</th>
																<th>ApiValue</th>
															</tr>
														</table>
													</div>
													
													<div class="col-md-12 sorguParametreAciklamasi" style="display:none">
														<div class="callout callout-info">
															<h4>Sorgu Parametreleri Açıklaması!</h4>
															<p><b>Title:</b> Sorgulama ekranına <i></>label</i> ekler</p>
															<p><b>Width:</b> Sorgulama ekranında grid<i>(x12)</i> genişliği</p>
															<p><b>Class:</b> Parametre için class özelliği ekler</p>
															<p><b>Value:</b> <i>Text</i> için default değer ataması, <i>SelectBox</i> için <i>option</i> değerleri atamasını sağlar ve kullanımı <b>0:Hayır|1:Evet</b> gibidir</i></p>
															<p><b>Api:</b> <i>SelectApi</i> için kullanılmalıdır ve <i>JSON</i> formatında olmalıdır</p>
															<p><b>ApiKey:</b> <i>SelectApi</i> için kullanılmalıdır ve <i>option</i> key özelliğini ifade eder</p>
															<p><b>ApiValue:</b> <i>SelectApi</i> için kullanılmalıdır ve <i>option</i> value özelliğini ifade eder</p>
															
														</div>
													</div>
												</div>
												<div style="clear:both"></div>
											</div>
											      
									      	<div role="tabpanel" class="tab-pane fade" id="js" aria-labelledby="profile-tab" style="padding:10px">
										         <div class="col-md-12">
													<label for="tmpJS">JavaScript</label>
													
														<c:if test="${empty dinamikRapor.js}">
<textarea id="tmpJS" name="js"> 
	$(function(){
		/*
		 * JS Code here..
		 */
		 
	});
</textarea>
														</c:if>
														<c:if test="${not empty dinamikRapor.js}">
														<textarea id="tmpJS" name="js">${dinamikRapor.js}</textarea>
														</c:if>
													
												</div>
												<div style="clear:both"></div>
									      	</div>
									   </div>
									</div>
							
								</form:form> 
									 
						</div>						

						<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_DINAMIK_RAPOR_ISLEMLERI_YAZMA')">
							<div class="row">
								<div class="col-md-4 col-md-offset-8">
									<div class="pull-right">
										<c:if test="${empty dinamikRapor.id}">
											<button class="btn bg-olive btn-flat margin btnSubmit">
												<i class="fa fa-save"></i> Kaydet
											</button>
										</c:if>
										<c:if test="${not empty dinamikRapor.id}">
											<button class="btn bg-orange btn-flat margin btnSubmit">
												<i class="fa fa-save"></i> Güncelle
											</button>
										</c:if>
	
									</div>
								</div>
							</div>
						</sec:authorize>
					</div>
				</div>
			</div>
		</sec:authorize>



		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Dinamik Rapor Listesi</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<table id="dataTable" class="table table-bordered table-striped dataTableClass">
									<thead>
										<tr>
											<th>ID</th>
											<th>Açıklama</th>
											<th>Kullanıcı</th>
											<th>Aktif Mi</th>
											<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_DINAMIK_RAPOR_ISLEMLERI_SILME')"><th>Silme</th></sec:authorize>
											<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_DINAMIK_RAPOR_ISLEMLERI_YAZMA')"><th>Düzeltme</th></sec:authorize>
											<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_DINAMIK_RAPOR_ISLEMLERI_YAZMA')"><th>Çalıştır</th></sec:authorize>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="tmp" items="${dinamikRaporList}">
											<tr>
												<td>${tmp.id}</td>
												<td>${tmp.aciklama}</td>
												<td>${tmp.kullanici}</td>
												<td>${tmp.durum ? 'Evet' : 'Hayır'}</td>
												<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_DINAMIK_RAPOR_ISLEMLERI_SILME')">
													<td width="30"><a
														href="${pageContext.request.contextPath}/admin/dinamik-rapor/sil/${tmp.id}"
														class="deleteRow" title="Sil"><i class="fa fa-trash-o"></i></a></td>
												</sec:authorize>
												
												<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_DINAMIK_RAPOR_ISLEMLERI_YAZMA')">
													<td width="30"><a
														href="${pageContext.request.contextPath}/admin/dinamik-rapor/guncelle/${tmp.id}"
														title="Düzenle"><i class="fa fa-edit"></i></a></td>
												</sec:authorize>
												<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_DINAMIK_RAPOR_ISLEMLERI_YAZMA')">
													<td width="30"><a
														href="${pageContext.request.contextPath}/admin/rapor/${tmp.id}"
														title="Çalıştır" target="_blank"><i class="fa fa-play"></i></a></td>
												</sec:authorize>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<%@include file="../template/adminFooter.jsp"%>

<script src="<c:url value="/assets/plugins/codemirror-5.10/lib/codemirror.js" />"></script>
<script src="<c:url value="/assets/plugins/codemirror-5.10/sql.js" />"></script>
<script src="<c:url value="/assets/plugins/codemirror-5.10/javascript.js" />"></script>
<script src="<c:url value="/assets/plugins/codemirror-5.10/addon/hint/show-hint.js" />"></script>
<script src="<c:url value="/assets/plugins/codemirror-5.10/addon/hint/sql-hint.js" />"></script>
<script src="<c:url value="/assets/plugins/codemirror-5.10/addon/comment/comment.js" />"></script>
<script src="<c:url value="/assets/plugins/codemirror-5.10/addon/comment/continuecomment.js" />"></script>
<script src="<c:url value="/assets/plugins/codemirror-5.10/addon/edit/matchbrackets.js" />"></script>


<script>

	$(function(){
		
		window.editor = CodeMirror.fromTextArea(document.getElementById('tmpSqlQuery'), {
		    mode: 'text/x-plsql',
		    indentWithTabs: true,
		    smartIndent: true,
		    lineNumbers: true,
		    matchBrackets : true,
		    autofocus: true,
		    extraKeys: {"Ctrl-Space": "autocomplete"},
		    hintOptions: {tables: {
		      users: {name: null, score: null, birthDate: null},
		      countries: {name: null, population: null, size: null}
		    }}
		  });
		
			
		CodeMirror.fromTextArea(document.getElementById("tmpJS"), {
			mode: 'text/javascript',
	        lineNumbers: true,
	        matchBrackets: true,
	        continueComments: "Enter",
	        extraKeys: {"Ctrl-Q": "toggleComment"}
	      });
		
		
		defaultParsing();
		
		$(".btnTemizle").click(function(e){
			e.preventDefault(); 
			window.editor.setValue("");
			$('.parametreTable tr').not(':first').remove();
			$('.parametreTable, .sorguParametreAciklamasi').hide();
		});
		
		$(".btnCozumle").click(function(e){
			e.preventDefault(); 
			parsing();
		});
		
		$('.jsPaging').change(function() {
		    if($(this).is(":checked"))
		    	$('.jsPageItemCount').prop('readonly', false);
		    else
		    	$('.jsPageItemCount').prop('readonly', true);
		});
		
		$(".btnSubmit").click(function(e){
			e.preventDefault(); 
					
			var data = new Array();
			
			$('.parametreTable tr').each(function(key, value){
				if(key != 0) {
					var tmp = 	{
	    					"_name"		: $(this).find('.nameIn').val(),
	    					"_type"		: $(this).find('.type').val(), 
	    					"_class"	: $(this).find('.class').val(), 
	    					"_width"	: $(this).find('.width').val(), 
	    					"_title"	: $(this).find('.title').val(), 
	    					"_value"	: $(this).find('.value').val(), 
	    					"_api"		: $(this).find('.api').val(), 
	    					"_apiKey"	: $(this).find('.apiKey').val(), 
	    					"_apiVal"	: $(this).find('.apiVal').val()
    					};
    				data.push(tmp);
				}
		    });
			
			$(".sqlQuery").val(window.editor.getValue());
			$(".formInputs").val(JSON.stringify(data));
			$(".yetki").val($(".yetkiSelect").val().toString());
			
		    if(!$('.jsPaging').is(":checked"))
		    	$('.jsPageItemCount').val(null);
						
			$('.dinamikRaporForm').submit();
		});
		
	});
	
	function unique(list) {
	    var result = [];
	    $.each(list, function(i, e) {
	        if ($.inArray(e, result) == -1) result.push(e);
	    });
	    return result;
	}
	
	function defaultParsing() {
		
		if($(".jsPaging").is(":checked"))
			$('.jsPageItemCount').prop('readonly', false);
		
		if($("#formInputs").val() != ""){
			var data = JSON.parse($("#formInputs").val());
			
			$.each(data, function(key, value) {
				
				var _name	= value._name.replace(':','');
				var _nameIn	= '<input type="text" class="form-control nameIn" readonly="readonly" value="'+ _name + '"/>';
				var _type 	= '<select class="form-control required type"><option value="text">Text</option><option value="date">Date</option><option value="select">SelectBox</option><option value="selectApi">SelectApi</option></select>';
				var _class 	= '<input type="text" class="form-control class" value="'+ value._class + '"  placeholder="required number .." />';
				var _width	= '<select class="form-control required width"><option value="1">1</option><option value="2">2</option><option value="3" selected="selected">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>';
				var _title 	= '<input type="text" class="form-control required title" value="'+ value._title + '"/>';
				var _value 	= '<input type="text" class="form-control value" value="'+ value._value + '" placeholder="0:Hayır|1:Evet"/>';
				var _api 	= '<input type="text" class="form-control api" value="'+ value._api + '" placeholder="prmHizmetTuru"/>';
				var _apiKey	= '<input type="text" class="form-control apiKey" value="'+ value._apiKey + '" placeholder="hizmetTuru"/>';
				var _apiVal	= '<input type="text" class="form-control apiVal" value="'+ value._apiVal + '" placeholder="aciklama"/>';
				
				var str = '<tr class="'+ value._name +'"><td> ' + _nameIn + ' </td><td> ' + _title 
						+ ' </td><td> ' + _type + ' </td><td> ' + _width + ' </td><td> ' + _class 
						+ ' </td><td> ' + _value + ' </td><td>' + _api + '</td><td> ' + _apiKey + ' </td><td> ' + _apiVal + ' </td></tr>';


				$('.parametreTable').append(str);
				$('.parametreTable, .sorguParametreAciklamasi').show();
				
				$('.parametreTable tr').last().find(".width").val(value._width);
				$('.parametreTable tr').last().find(".type").val(value._type);
				
		    });
		}
		
	}
	
	function parsing() {
		
		var _type 	= '<select class="form-control required type"><option value="text">Text</option><option value="date">Date</option><option value="select">SelectBox</option><option value="selectApi">SelectApi</option></select>';
		var _class 	= '<input type="text" class="form-control class" placeholder="required number .."/>';
		var _width	= '<select class="form-control required width"><option value="1">1</option><option value="2">2</option><option value="3" selected="selected">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>';
		var _title 	= '<input type="text" class="form-control required title" />';
		var _value 	= '<input type="text" class="form-control value" placeholder="0:Hayır|1:Evet"/>';
		var _api 	= '<input type="text" class="form-control api" placeholder="prmHizmetTuru"/>';
		var _apiKey	= '<input type="text" class="form-control apiKey" placeholder="hizmetTuru"/>';
		var _apiVal	= '<input type="text" class="form-control apiVal" placeholder="aciklama"/>';
		
		var txt = window.editor.getValue();
	    var regExp = /:([a-zA-Z0-9])+/gi;
	    
	    
	    if(txt.match(regExp) != null){
	    	var inputs = unique(txt.match(regExp));
	    	
	    	$.each(inputs, function(key, value) {
		    	var _name	= value.replace(':','');
				var _nameIn	= '<input type="text" class="form-control nameIn" readonly="readonly" value="'+ _name + '" />';
				var str = '<tr class="'+ _name +'"><td> ' + _nameIn + ' </td><td> ' + _title 
						+ ' </td><td> ' + _type + ' </td><td> ' + _width + ' </td><td> ' + _class 
						+ ' </td><td> ' + _value + ' </td><td>' + _api + '</td><td> ' + _apiKey + ' </td><td> ' + _apiVal + ' </td></tr>';
				
				if(!$('.parametreTable tr').hasClass(_name))
				{
					$('.parametreTable').append(str);
					$('.parametreTable, .sorguParametreAciklamasi').show();
				}
					
		    });
		    
		    $('.parametreTable tr').each(function(key, value){
		    	if(key != 0 && inputs.indexOf(":" + $(this).attr("class")) == -1){
		    		$(this).remove();
		    	}
		    });
	    	
	    } else{
	    	$('.parametreTable tr').not(':first').remove();
			$('.parametreTable, .sorguParametreAciklamasi').hide();
	    }
	    
	}
	
	
</script>

