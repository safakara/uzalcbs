<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page session="true" contentType="text/html; charset=UTF-8"%>
<%@include file="../template/header.jsp"%>

<title>UzalCBS | Kullanıcı Kısıtlama İşlemleri</title>
<%@include file="../template/menu.jsp"%>

<div class="menu-selector" data-item="webModulePermission"></div>
<div class="content-wrapper" style="min-height: 1066px;">

	<section class="content">
		
		<c:set var="formURL" scope="session" value="${pageContext.request.contextPath}/admin/web-module-permission/edit" />
		<form:form method="POST" cssClass="formGonder" commandName="webModulePermissionForm" action="${formURL}">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-success">
						<div class="box-header with-border">
							<h3 class="box-title">Kullanıcı Kısıtlama İşlemleri</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<c:if test="${not empty msg}">
										<div class="col-md-12">
											<div class="callout callout-success">
												<h4><i class="fa fa-check-circle"></i> Başarılı</h4>
												<p>${msg}</p>
											</div>
										</div>
									</c:if>
								</div>
								
								<div class="col-md-3">
									<div class="form-group">
										<label for="roleId">Role Seçiniz:</label> 
										<select
											data-placeholder=" " name="roleId"
											class="form-control roleId autoComplate"
											data-selected="${webModulePermissionForm.roleId}"
											data-first-empty="true" data-url="webRoles"
											data-value="id" data-text="name" required>
										</select>
										<input type="hidden" name="kontrol" class="kontrol" value="${webModulePermissionForm.kontrol}" />
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									</div>
								</div>
								<div class="col-md-9"></div>
							
							
								<div class="col-md-12">
									<c:if test="${not empty webModulePermissionForm.roleId}">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>Role Adı</th>
													<th>Module Adı</th>
													<th>Okuma</th>
													<th>Yazma</th>
													<th>Silme</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="tmpList" items="${webModulePermissionFormList}" varStatus="l"> 
													<tr>
														<td>${tmpList.roleAdi}</td>
														<td>${tmpList.moduleAdi}</td>
														<td><input type="checkbox" name="permissions" value="${l.index}_0" <c:if test="${tmpList.okuma}">checked</c:if> /> </td>
														<td><input type="checkbox" name="permissions" value="${l.index}_1" <c:if test="${tmpList.yazma}">checked</c:if> /> </td>
														<td><input type="checkbox" name="permissions" value="${l.index}_2" <c:if test="${tmpList.silme}">checked</c:if> /> </td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_KULLANICI_KISITLAMA_ISLEMLERI_YAZMA')">
											<div class="col-md-4 col-md-offset-8">
												<div class="pull-right">
													<sec:authorize access="hasAnyRole('ROLE_ADMIN','PERM_KULLANICI_KISITLAMA_ISLEMLERI_SILME')">
														<a href="${pageContext.request.contextPath}/admin/web-module-permission" class="btn  btn-default btn-flat"><i class="fa fa-repeat"></i> Temizle</a>
													</sec:authorize>
													<button type="submit" class="btn bg-olive btn-flat margin"><i class="fa fa-save"></i> Kaydet</button>
												</div>
											</div>
										</sec:authorize>
									</c:if>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</div>
	
	
		</form:form>
	</section>
</div>


<%@include file="../template/adminFooter.jsp"%>

<script type="text/javascript">
	$(function(){
		$('.roleId').on('change',function(e) {
			$(".kontrol").val(0);
			$(".formGonder").submit();
		});
	});
</script>


