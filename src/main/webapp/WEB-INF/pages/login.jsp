<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true" contentType="text/html; charset=UTF-8"%>
<%@include file="../template/header.jsp" %>

<title>UzalCBS | Login</title>
<body class="login-page">

<div id="login-box">
	
	<div class="login-box">
      <div class="login-logo">
        <img width="180" style="border:4px solid #eee" src="${pageContext.request.contextPath}/assets/img/logo.png" class="img-circle">
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Lütfen giriş yapınız.</p>
        <c:if test="${not empty error}">
				<div class="alert alert-warning alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-warning"></i> Uyarı!</h4>
					${error}
				</div>
		</c:if>
        
        <c:if test="${not empty msg}">
        	<div class="alert alert-info alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					${msg}
				</div>
		</c:if>
        
        <form name='loginForm' action="<c:url value='/login' />" method='POST'>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Email" name="username" required/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Şifre" name="password" required/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
                     
          <div class="row">
            <div class="col-xs-8">    
              <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />                  
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Giriş</button>
            </div><!-- /.col -->
          </div>
        </form>

      </div>
    </div>
	
	</form>
</div>

<%@include file="../template/footer.jsp" %>

