<script src="<c:url value="/assets/plugins/jQuery/jQuery-2.1.4.min.js" />"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<c:url value="/assets/js/bootstrap.min.js" />" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<c:url value="/assets/plugins/iCheck/icheck.min.js" />" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>