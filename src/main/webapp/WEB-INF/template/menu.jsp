<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@include file="navbar.jsp"%>

<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar" style="height: auto;">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<c:url value="/assets/img/logo.png" />"
					class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p class="domain" style="padding-top: 10px;">@${kullaniciAdi}</p>
			</div>
		</div>

		<ul class="sidebar-menu">
			<li class="anasayfa"><a
				href="${pageContext.request.contextPath}/admin/"><i
					class="fa fa-th"></i> <span>Anasayfa</span></a></li>

			<sec:authorize
				access="hasAnyRole('ROLE_ADMIN','OPERASYON_OKUMA')">
				<li
					class="treeview smarttbillOperasyon ttvmOperasyon testOrtamiSVDOlustur">
					<a href="#"> <i class="fa fa-edit"></i> <span>Veri Girişi</span> <i
						class="fa fa-angle-left pull-right"></i>
				</a>
					<ul
						class="treeview-menu smarttbillOperasyon ttvmOperasyon testOrtamiSVDOlustur ohat">
						<sec:authorize
							access="hasAnyRole('ROLE_ADMIN','SMARTTBILL_OPERASYON_OKUMA')">
							<li class="smarttbillOperasyon"><a
								href="${pageContext.request.contextPath}/admin/smarttbill-operasyon"><i
									class="fa fa-circle-o"></i> İstasyon Verileri</a></li>
						</sec:authorize>

						<sec:authorize
							access="hasAnyRole('ROLE_ADMIN','TTVM_OPERASYON_OKUMA')">
							<li class="ttvmOperasyon"><a
								href="${pageContext.request.contextPath}/admin/ttvm-operasyon"><i
									class="fa fa-circle-o"></i> Sediment Verileri</a></li>
						</sec:authorize>

						<sec:authorize
							access="hasAnyRole('ROLE_ADMIN','TEST_ORTAMI_SVD_OLUSTUR_YAZMA')">
							<li class="testOrtamiSVDOlustur"><a
								href="${pageContext.request.contextPath}/admin/test-ortami-svd-olustur"><i
									class="fa fa-circle-o"></i> Su Analiz Verileri</a></li>
						</sec:authorize>
						
						<sec:authorize
							access="hasAnyRole('ROLE_ADMIN','OHAT_YAZMA')">
							<li class="ohat"><a
								href="${pageContext.request.contextPath}/admin/ohat"><i
									class="fa fa-circle-o"></i> Prop Verileri</a></li>
						</sec:authorize>
						
						<sec:authorize
							access="hasAnyRole('ROLE_ADMIN','OHAT_YAZMA')">
							<li class="ohat"><a
								href="${pageContext.request.contextPath}/admin/ohat"><i
									class="fa fa-circle-o"></i> Laboratuvar Verileri</a></li>
						</sec:authorize>
					</ul>
				</li>
			</sec:authorize>

			<sec:authorize	access="hasAnyRole('ROLE_ADMIN','HARITALAR_OKUMA')">
				<li	class="treeview haritalar harita">
					<a href="#"> 
						<i class="fa fa-map-marker"></i> <span>Haritalar</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul	class="treeview-menu harita">
							<li class="harita">
								<a href="${pageContext.request.contextPath}/admin/harita">
									<i class="fa fa-circle-o"></i> Harita
								</a>
							</li>
						
					</ul>
				</li>
			</sec:authorize>
						

			<sec:authorize
				access="hasAnyRole('ROLE_ADMIN','RAPORLAMA_OKUMA')">
				<li
					class="treeview raporlama tutanakRaporu icmalListesi
				<c:forEach var="tmp" items="${dinamikRaporList}">
					r${tmp.id}	
				</c:forEach>
				"><a
					href="#"> <i class="fa fa-table"></i> <span>Raporlama</span> <i
						class="fa fa-angle-left pull-right"></i>
				</a>
					<ul class="treeview-menu raporlama">
						
						<sec:authorize
							access="hasAnyRole('ROLE_ADMIN','DINAMIK_RAPOR_OLUSTUR_YAZMA')">
							<li class="dinamikRaporIslemleri"><a
								href="${pageContext.request.contextPath}/admin/dinamik-rapor/olustur"><i
									class="fa fa-circle-o"></i> Dinamik Rapor İşlemleri</a></li>
						</sec:authorize>


						<c:forEach var="tmp" items="${dinamikRaporList}">
							<li class="r${tmp.id} }">
								<a href="${pageContext.request.contextPath}/admin/rapor/${tmp.id}">
									<i class="fa fa-circle-o"></i> ${tmp.aciklama}
								</a>
							</li>
						</c:forEach>

					</ul></li>
			</sec:authorize>
	
			<sec:authorize	access="hasAnyRole('ROLE_ADMIN','KULLANICI_ISLEMLERI_OKUMA')">
				<li	class="treeview kullaniciIslemleri kullanici kullaniciGrup">
					<a href="#"> 
						<i class="fa fa-users"></i> <span>Kullanıcı İşlemleri</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul	class="treeview-menu kullanici kullaniciGrup">
						<sec:authorize access="hasAnyRole('ROLE_ADMIN','KULLANICI_OKUMA')">
							<li class="kullanici">
								<a href="${pageContext.request.contextPath}/admin/kullanici">
									<i class="fa fa-circle-o"></i> Kullanıcı
								</a>
							</li>
						</sec:authorize>
						<sec:authorize access="hasAnyRole('ROLE_ADMIN','KULLANICI_GRUP_OKUMA')">
							<li class="kullaniciGrup">
								<a href="${pageContext.request.contextPath}/admin/kullanici-grup">
									<i class="fa fa-circle-o"></i> Kullanıcı Grup
								</a>
							</li>
						</sec:authorize>
						
					</ul>
				</li>
			</sec:authorize>

		</ul>
	</section>
</aside>
