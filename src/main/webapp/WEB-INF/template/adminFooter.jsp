<%@page session="true" contentType="text/html; charset=UTF-8"%>

<c:if test="${control != 1}">
	<footer style="height: 50px" class="main-footer">
		<div class="pull-right hidden-xs">
			<b>UzalCBS</b> © 2017
		</div>
	</footer>
</c:if>

<script src="<c:url value="/assets/plugins/jQuery/jQuery-2.1.4.min.js" />"></script>

<script src="<c:url value="/assets/js/bootstrap.min.js" />" type="text/javascript"></script>

<script src='<c:url value="/assets/plugins/fastclick/fastclick.min.js" />'></script>

<script src="<c:url value="/assets/js/app.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/sparkline/jquery.sparkline.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/slimScroll/jquery.slimscroll.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/chartjs/Chart.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/datatables/jquery.dataTables.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/datatables/dataTables.bootstrap.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/bootbox/bootbox.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/chosen/js/chosen.jquery.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/timepicker/bootstrap-timepicker.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/inputmask.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/jquery.inputmask.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/inputmask.date.extensions.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/inputmask.dependencyLib.jquery.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/inputmask.extensions.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/inputmask.numeric.extensions.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/inputmask.phone.extensions.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/inputmask.regex.extensions.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/input-mask/jquery.inputmask.bundle.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/datetimepicker/js/moment-with-locales.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/datetimepicker/js/bootstrap-datetimepicker.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/flot/jquery.flot.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/flot/jquery.flot.pie.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/assets/plugins/flot/jquery.flot.categories.min.js" />" type="text/javascript"></script>





<script type="text/javascript">
	$(function() {

		$('.dataTableClass').dataTable(
				{
					"lengthMenu" : [ [ 10, 25, 50, 100, -1 ],
							[ 10, 25, 50, 100, "Tümü" ] ]
				});

		//modal kontrol
		var control = $(".control").html();
		if (control == '1' || control == undefined) {
			/*------- DataTable ayarları -------*/
			var elements = $('.dataTableClass');
			for (var i = 0; i < elements.length; i++) {
				var id = elements[i].id;
				$('#' + id + ', .datatable2').dataTable();
			}
			/*----------------------------------*/

			$('footer').eq(1).remove();
		}

		/*------- Required ayarları -------*/
		$(".required").prop('required', true);
		/*----------------------------------*/

		/*------- DatePicker ayarları -------*/
		$('.datetimepicker').datetimepicker({
			locale : 'tr'
		});

		$('.datepicker').datetimepicker({
			locale : 'tr',
			format : 'DD.MM.YYYY'
		});
		/*----------------------------------*/

		/*------- Form Submit ayarları -------*/
		$('form').submit(function() {
			$("form input[type=text]").each(function() {
				if ($(this).val() == "" || $(this).val() == undefined)
					$(this).attr("name", "");
			});

		});
		/*----------------------------------*/

		/*------- Menü Selector ayarları -------*/
		var menuSelector = "." + $(".menu-selector").attr("data-item");
		$(menuSelector).addClass("active");
		//alert(menuSelector);
		//if($(menuSelector).parent().hasClass("treeview-menu"))
		//$(menuSelector).parent().hasClass("treeview-menu").addClass("menu-open").show();
		/*----------------------------------*/

		/*------- Mask ayarları -------*/
		$(".date").inputmask("dd/mm/yyyy", {
			"placeholder" : "dd/mm/yyyy"
		});

		$(".number")
				.each(
						function() {
							var precision = $(this).attr("data-precision") != undefined ? $(
									this).attr("data-precision")
									: 15;
							var subPrecision = $(this).attr(
									"data-sub-precision") === undefined ? 0
									: $(this).attr("data-sub-precision");
							var pattern = "";
							var text = "";

							while (precision > 0) {
								pattern += "9";
								precision--;
							}

							for (i = 0; subPrecision > i; i++) {
								if (i == 0) {
									pattern += ".";
								}

								pattern += "9";
							}

							$(this).inputmask(pattern, {
								"placeholder" : text
							});
						});

		$("[data-mask]").inputmask();

		/*----------------------------------*/

		/*-------- Delete Confirm ----------*/
		$(document).on('click', '.deleteRow', function(e) {
			var link = $(this).attr("href");
			e.preventDefault();
			bootbox.confirm("Silmek istiyor musunuz?", function(result) {
				if (result) {
					document.location.href = link;
				}
			});
		});
		/*----------------------------------*/

		/*--------- Autocomplate Ayarları ---------*/
		$('.autoComplate')
				.each(
						function() {
							var dataFirstEmpty = $(this).attr(
									"data-first-empty");
							var dataSelected;

							if ($(this).attr("data-selected") === undefined
									|| $(this).attr("data-selected") === null) {
								dataSelected = [];
							} else {
								dataSelected = $(this).attr("data-selected")
										.split(",");
							}

							var dataUrl = $(this).attr("data-url");
							var dataValue = $(this).attr("data-value");
							var dataText = $(this).attr("data-text");
							var result = [];
							var selected = [];

							$.ajaxSetup({
								async : false
							});

							$.getJSON('${pageContext.request.contextPath}/api/'
									+ dataUrl, function(data) {
								if (dataFirstEmpty == "true")
									result.push("<option> </option>");

								$.each(data, function(key, value) {
									var tmp = "";
									var textTmp = "";
									var valueTmp = "";
									$.each(value, function(k, v) {
										if (k == dataValue)
											valueTmp = v;
										else if (k == dataText)
											textTmp = v;
									});
									if (dataValue == dataText)
										textTmp = valueTmp;

									if (dataSelected.indexOf(valueTmp
											.toString()) > -1)
										selected.push(key + 1);

									tmp = "<option value=\"" + valueTmp + "\">"
											+ textTmp + "</option>";
									result.push(tmp);
								});
							});

							$.each(selected, function(index, value) {
								result[value] = result[value].replace("\">",
										"\" selected='selected'>");
							});

							var tmp = "";

							$.each(result, function(i, v) {
								tmp += v;
							});

							$(this).empty().append(tmp).chosen().trigger(
									"chosen:updated");

							$(".chosen-container").css("width", "100%");

						});
		/*-----------------------------------------*/

	});

	function labelFormatter(label, series) {
		return "<div style='font-size:13px; text-align:center; padding:2px; color: #444; font-weight: 600;'>"
				+ label
				+ "<br/>"
				+ Math.round(series.percent * 100)
				/ 100
				+ "%</div>";
	}

	var tableToExcel = (function() {
		var uri = 'data:application/vnd.ms-excel;base64,', template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', base64 = function(
				s) {
			return window.btoa(unescape(encodeURIComponent(s)))
		}, format = function(s, c) {
			return s.replace(/{(\w+)}/g, function(m, p) {
				return c[p];
			})
		}
		return function(table, name) {
			if (!table.nodeType)
				table = document.getElementById(table)
			var ctx = {
				worksheet : name || 'Worksheet',
				table : table.innerHTML
			}
			window.location.href = uri + base64(format(template, ctx))
		}
	})();
</script>

</body>
</html>