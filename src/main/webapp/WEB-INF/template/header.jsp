<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta charset="UTF-8">
    <link href="<c:url value="/assets/css/bootstrap.min.css" />" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/css/AdminLTE.min.css" />" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/plugins/iCheck/square/blue.css" />" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/plugins/datatables/dataTables.bootstrap.css" />" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/css/skins/_all-skins.min.css" />" rel="stylesheet" type="text/css" />
    
    <link href="<c:url value="/assets/plugins/chosen/css/chosen.min.css" />" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/plugins/chosen/css/chosen-bootstrap.css" />" rel="stylesheet" type="text/css" />
	<link href="<c:url value="/assets/plugins/timepicker/bootstrap-timepicker.min.css" />" rel="stylesheet"/>
	<link href="<c:url value="/assets/plugins/datetimepicker/css/bootstrap-datetimepicker.css" />" rel="stylesheet"/>
	<link rel='stylesheet' href='<c:url value="/assets/plugins/colorpicker/bootstrap-colorpicker.min.css"/>'>
	<!-- link href="<c:url value="/assets/css/main.css" />" rel="stylesheet"/> -->
	<link href="<c:url value="/assets/img/logo.png" />" rel="shortcut icon" type="image/x-icon" />
	
	
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  