package tr.edu.cu.uzalcbs.aop;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ServiceAspect {
	
	
	//@Before("execution(* com.tt.smarttweb.service.*.*())")
	public void serviceAdvice(){
		System.out.println("\n\n\n  aop deneme");
	}
	
	
}
