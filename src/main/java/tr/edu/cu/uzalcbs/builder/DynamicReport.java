package tr.edu.cu.uzalcbs.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import tr.edu.cu.uzalcbs.form.DinamikRaporFormInputs;
import tr.edu.cu.uzalcbs.model.DinamikRapor;
import tr.edu.cu.uzalcbs.service.DinamikRaporService;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class DynamicReport {

	public DynamicReport(Builder builder) {
		this.dinamikRapor = builder.dinamikRapor;
		this.dinamikRaporService = builder.dinamikRaporService;
		this.request = builder.request;
	}

	private DinamikRapor dinamikRapor;
	private DinamikRaporService dinamikRaporService;
	private HttpServletRequest request;

	public static class Builder {
		private DinamikRapor dinamikRapor;
		private DinamikRaporService dinamikRaporService;
		private HttpServletRequest request;

		public Builder(DinamikRapor dinamikRapor, DinamikRaporService dinamikRaporService, HttpServletRequest request) {
			this.dinamikRapor = dinamikRapor;
			this.dinamikRaporService = dinamikRaporService;
			this.request = request;
		}

		public void Js(String js) {
			this.dinamikRapor.setJs(js);
		}

		public void QueryAutoExec(Boolean queryAutoExec) {
			this.dinamikRapor.setQueryAutoExec(queryAutoExec);
		}

		public void JsPaging(Boolean jsPaging) {
			this.dinamikRapor.setJsPaging(jsPaging);
		}

		public void JsPageItemCount(Integer jsPageItemCount) {
			this.dinamikRapor.setJsPageItemCount(jsPageItemCount);
		}

		public DynamicReport build() {
			return new DynamicReport(this);
		}
	}

	private List<DinamikRaporFormInputs> getInputList() {

		// [{"_name":"ucretBandi","_type":"text","_class":"required","_width":"4","_title":"Ücret Bandı","_value":"1:Aylık|2:Günlük","_api":"","_apiKey":"","_apiVal":""}]

		List<DinamikRaporFormInputs> inputList = new ArrayList<DinamikRaporFormInputs>();

		if (this.dinamikRapor.getFormInputs() != null && !this.dinamikRapor.getFormInputs().trim().isEmpty()) {
			HashMap<String, String> arrList = new HashMap<String, String>();
			JsonParser parser = new JsonParser();
			JsonArray arr = (JsonArray) parser.parse(this.dinamikRapor.getFormInputs());

			for (int i = 0; i < arr.size(); i++) {
				DinamikRaporFormInputs formInputs = new DinamikRaporFormInputs();

				JsonObject element = (JsonObject) arr.get(i);

				formInputs.set_api(element.get("_api").toString().replace("\"", ""));
				formInputs.set_apiKey(element.get("_apiKey").toString().replace("\"", ""));
				formInputs.set_apiVal(element.get("_apiVal").toString().replace("\"", ""));
				formInputs.set_class(element.get("_class").toString().replace("\"", ""));
				formInputs.set_name(element.get("_name").toString().replace("\"", ""));
				formInputs.set_title(element.get("_title").toString().replace("\"", ""));
				formInputs.set_type(element.get("_type").toString().replace("\"", ""));
				formInputs.set_width(Integer.parseInt(element.get("_width").toString().replace("\"", "")));

				boolean isStr = true;

				if (StringUtils.containsIgnoreCase(element.get("_value").toString(), "|")) {
					for (String tmpStr : element.get("_value").toString().replace("\"", "").trim().split("\\|")) {
						String[] tmp = tmpStr.split("\\:");
						arrList.put(tmp[0], tmp[1]);
						isStr = false;
					}
				}

				if (isStr) {
					formInputs.set_valueStr(element.get("_value").toString().replace("\"", ""));
				} else {
					formInputs.set_valueArr(arrList);
				}

				inputList.add(formInputs);

			}
		}
		return inputList;
	}

	public String getRawData() {

		List<DinamikRaporFormInputs> inputList = getInputList();
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		ArrayList<Object> result = new ArrayList<Object>();

		if ("POST".equalsIgnoreCase(this.request.getMethod())) {
			for (DinamikRaporFormInputs tmp : inputList) {
				parameters.put(tmp.get_name(), this.request.getParameter(tmp.get_name()));
			}
		}

		if (this.dinamikRapor.getQueryAutoExec() || "POST".equalsIgnoreCase(this.request.getMethod())) {
			result = (ArrayList<Object>) this.dinamikRaporService.getDynamicReport(this.dinamikRapor.getSqlQuery(), parameters);
		}

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < result.size(); i++) {
			HashMap<String, Object> columnNames = (HashMap<String, Object>) result.get(i);
			for (Entry<String, Object> e : columnNames.entrySet()) {
				if (e.getValue() != null)
					sb.append(e.getValue() + " ");
			}
		}

		return sb.toString();
	}

	public String getTable() {

		List<DinamikRaporFormInputs> inputList = getInputList();
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		ArrayList<Object> result = new ArrayList<Object>();

		if ("POST".equalsIgnoreCase(this.request.getMethod())) {
			for (DinamikRaporFormInputs tmp : inputList) {
				parameters.put(tmp.get_name(), this.request.getParameter(tmp.get_name()));
			}
		}

		if (this.dinamikRapor.getQueryAutoExec() || "POST".equalsIgnoreCase(this.request.getMethod())) {
			result = (ArrayList<Object>) this.dinamikRaporService.getDynamicReport(this.dinamikRapor.getSqlQuery(), parameters);
		}

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < result.size(); i++) {
			HashMap<String, Object> columnNames = (HashMap<String, Object>) result.get(i);
			if (i == 0) {
				sb.append("<table id=\"reportTable\" class=\"table table-bordered table-striped\">");
				sb.append("<thead><tr>");

				for (Entry<String, Object> e : columnNames.entrySet()) {
					sb.append("<th>");
					sb.append(e.getKey());
					sb.append("</th>");
				}

				sb.append("</tr></thead>");
				sb.append("<tbody>");

			}

			sb.append("<tr>");

			for (Entry<String, Object> e : columnNames.entrySet()) {
				sb.append("<td>");
				if (e.getValue() != null)
					sb.append(e.getValue());
				sb.append("</td>");
			}

			sb.append("</tr>");

			if (result.size() - i == 1) {
				sb.append("</tbody>");
				sb.append("</table>");
			}

		}

		return sb.toString();
	}

	public String getInputForm() {

		List<DinamikRaporFormInputs> inputList = getInputList();
		HashMap<String, Object> parameters = new HashMap<String, Object>();

		StringBuilder sb = new StringBuilder();

		if ("POST".equalsIgnoreCase(this.request.getMethod())) {
			for (DinamikRaporFormInputs tmp : inputList) {
				parameters.put(tmp.get_name(), this.request.getParameter(tmp.get_name()));
			}
		}

		for (DinamikRaporFormInputs input : inputList) {

			if ("text".equalsIgnoreCase(input.get_type())) {
				sb.append("<div class=\"col-md-");
				sb.append(input.get_width());
				sb.append("\"><div class=\"form-group\"><label for=\"");
				sb.append(input.get_name());
				sb.append("\">");
				sb.append(input.get_title());
				sb.append("</label>");
				sb.append("<input id=\"");
				sb.append(input.get_name());
				sb.append("\" type=\"text\" class=\"form-control ");
				sb.append(input.get_class());
				sb.append("\" name=\"");
				sb.append(input.get_name());
				if (parameters.get(input.get_name()) != null) {
					sb.append("\" value=\"");
					sb.append(parameters.get(input.get_name()));
				}
				sb.append("\"></div></div>");

			} else if ("date".equalsIgnoreCase(input.get_type())) {
				sb.append("<div class=\"col-md-");
				sb.append(input.get_width());
				sb.append("\"><div class=\"form-group\"><label for=\"");
				sb.append(input.get_name());
				sb.append("\">");
				sb.append(input.get_title());
				sb.append("</label>");
				sb.append("<div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></div>");
				sb.append("<input id=\"");
				sb.append(input.get_name());
				sb.append("\" type=\"text\" data-inputmask=\"'alias': 'dd/mm/yyyy'\" data-mask=\"\" class=\"form-control ");
				sb.append(input.get_class());
				sb.append("\" name=\"");
				sb.append(input.get_name());
				if (parameters.get(input.get_name()) != null) {
					sb.append("\" value=\"");
					sb.append(parameters.get(input.get_name()));
				}
				sb.append("\"></div></div></div>");

			} else if ("select".equalsIgnoreCase(input.get_type())) {
				sb.append("<div class=\"col-md-");
				sb.append(input.get_width());
				sb.append("\"><div class=\"form-group\"><label for=\"");
				sb.append(input.get_name());
				sb.append("\">");
				sb.append(input.get_title());
				sb.append("</label>");
				sb.append("<select id=\"");
				sb.append(input.get_name());
				sb.append("\" class=\"form-control ");
				sb.append(input.get_class());
				sb.append("\" name=\"");
				sb.append(input.get_name());
				sb.append("\">");
				for (Entry<String, String> e : input.get_valueArr().entrySet()) {
					sb.append("<option value=\"");
					sb.append(e.getKey());
					sb.append("\" ");

					if (parameters.get(input.get_name()) != null && e.getKey().equalsIgnoreCase(parameters.get(input.get_name()).toString()))
						sb.append("selected");

					sb.append(">");
					sb.append(e.getValue());
					sb.append("</option>");
				}

				sb.append("</select></div></div>");

			} else if ("selectApi".equalsIgnoreCase(input.get_type())) {
				sb.append("<div class=\"col-md-");
				sb.append(input.get_width());
				sb.append("\"><div class=\"form-group\"><label for=\"");
				sb.append(input.get_name());
				sb.append("\">");
				sb.append(input.get_title());
				sb.append("</label>");
				sb.append("<select id=\"");
				sb.append(input.get_name());
				sb.append("\" data-first-empty=\"true\" class=\"form-control autoComplate ");
				sb.append(input.get_class());
				sb.append("\" name=\"");
				sb.append(input.get_name());
				sb.append("\" data-url=\"");
				sb.append(input.get_api());
				sb.append("\" data-value=\"");
				sb.append(input.get_apiKey());
				sb.append("\" data-text=\"");
				sb.append(input.get_apiVal());
				sb.append("\" data-selected=\"");
				sb.append(parameters.get(input.get_name()));
				sb.append("\">");

				sb.append("</select></div></div>");

			}

		}

		return sb.toString();

	}

}
