package tr.edu.cu.uzalcbs.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tr.edu.cu.uzalcbs.model.KullaniciGrup;
import tr.edu.cu.uzalcbs.service.KullaniciGrupService;

@RestController
@RequestMapping(value="/api")
public class KullaniciGrupApi {
	
	@Autowired
	private KullaniciGrupService kullaniciGrupService;
	
	@RequestMapping(value = "/kullanici-grup", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getKullaniciGrup() {
		List<KullaniciGrup> result = kullaniciGrupService.getKullaniciGrupList().stream()
									.map(p-> {
												p.setKullanicis(null);
												p.setSayfaYetkileris(null);
												return p;
									})
									.collect(Collectors.toList());
		
		return new ResponseEntity(result, HttpStatus.OK);
	}
	
}
