package tr.edu.cu.uzalcbs.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tr.edu.cu.uzalcbs.model.Kullanici;
import tr.edu.cu.uzalcbs.service.KullaniciService;

@RestController
@RequestMapping(value="/api")
public class KullaniciApi {

	@Autowired
	private KullaniciService kullaniciService;
	
	@RequestMapping(value = "/kullanici", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> getKullaniciGrup() {
		List<Kullanici> result = kullaniciService.getKullaniciList().stream()
									.map(p-> {
												p.setSifre(null);
												p.getKullaniciGrup().setKullanicis(null);
												p.getKullaniciGrup().setSayfaYetkileris(null);
												return p;
									})
									.collect(Collectors.toList());
		
		return new ResponseEntity(result, HttpStatus.OK);
	}
	
}
