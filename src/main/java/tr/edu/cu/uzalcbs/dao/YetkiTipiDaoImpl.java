package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.YetkiTipi;

@Repository
public class YetkiTipiDaoImpl implements YetkiTipiDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public YetkiTipi getYetkiTipi(Integer id) {
		YetkiTipi yetkiTipi = (YetkiTipi) getCurrentSession().get(YetkiTipi.class, id);
		return yetkiTipi;
	}

	@Override
	public void addYetkiTipi(YetkiTipi yetkiTipi) {
		getCurrentSession().save(yetkiTipi);
	}

	@Override
	public void updateYetkiTipi(YetkiTipi yetkiTipi) {
		getCurrentSession().update(yetkiTipi);
	}
	
	@Override
	public void deleteYetkiTipi(Integer id) {
		YetkiTipi yetkiTipi = getYetkiTipi(id);
		if (yetkiTipi != null)
			getCurrentSession().delete(yetkiTipi);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<YetkiTipi> getYetkiTipiList() {
		return getCurrentSession().createCriteria(YetkiTipi.class).list();
	}
	
}
