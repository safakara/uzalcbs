package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import tr.edu.cu.uzalcbs.model.KimyasalMadde;

public interface KimyasalMaddeDao {
	
	public KimyasalMadde getKimyasalMadde(Integer id);
	public void addKimyasalMadde(KimyasalMadde kimyasalMadde);
	public void updateKimyasalMadde(KimyasalMadde kimyasalMadde);
	public void deleteKimyasalMadde(Integer id);
	public List<KimyasalMadde> getKimyasalMaddeList();

}
