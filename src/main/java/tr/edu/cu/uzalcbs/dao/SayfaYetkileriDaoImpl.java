package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.SayfaYetkileri;

@Repository
public class SayfaYetkileriDaoImpl implements SayfaYetkileriDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public SayfaYetkileri getSayfaYetkileri(Integer id) {
		SayfaYetkileri sayfaYetkileri = (SayfaYetkileri) getCurrentSession().get(SayfaYetkileri.class, id);
		return sayfaYetkileri;
	}

	@Override
	public void addSayfaYetkileri(SayfaYetkileri sayfaYetkileri) {
		getCurrentSession().save(sayfaYetkileri);
	}

	@Override
	public void updateSayfaYetkileri(SayfaYetkileri sayfaYetkileri) {
		getCurrentSession().update(sayfaYetkileri);
	}
	
	@Override
	public void deleteSayfaYetkileri(Integer id) {
		SayfaYetkileri sayfaYetkileri = getSayfaYetkileri(id);
		if (sayfaYetkileri != null)
			getCurrentSession().delete(sayfaYetkileri);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SayfaYetkileri> getSayfaYetkileriList() {
		return getCurrentSession().createCriteria(SayfaYetkileri.class).list();
	}
	
}
