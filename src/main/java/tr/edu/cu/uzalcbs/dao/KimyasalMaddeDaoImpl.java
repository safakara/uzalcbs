package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.KimyasalMadde;

@Repository
public class KimyasalMaddeDaoImpl implements KimyasalMaddeDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public KimyasalMadde getKimyasalMadde(Integer id) {
		KimyasalMadde kimyasalMadde = (KimyasalMadde) getCurrentSession().get(KimyasalMadde.class, id);
		return kimyasalMadde;
	}

	@Override
	public void addKimyasalMadde(KimyasalMadde kimyasalMadde) {
		getCurrentSession().save(kimyasalMadde);
	}

	@Override
	public void updateKimyasalMadde(KimyasalMadde kimyasalMadde) {
		getCurrentSession().update(kimyasalMadde);
	}
	
	@Override
	public void deleteKimyasalMadde(Integer id) {
		KimyasalMadde kimyasalMadde = getKimyasalMadde(id);
		if (kimyasalMadde != null)
			getCurrentSession().delete(kimyasalMadde);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<KimyasalMadde> getKimyasalMaddeList() {
		return getCurrentSession().createCriteria(KimyasalMadde.class).list();
	}
	
}
