package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.Istasyon;

@Repository
public class IstasyonDaoImpl implements IstasyonDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public Istasyon getIstasyon(Integer id) {
		Istasyon istasyon = (Istasyon) getCurrentSession().get(Istasyon.class, id);
		return istasyon;
	}

	@Override
	public void addIstasyon(Istasyon istasyon) {
		getCurrentSession().save(istasyon);
	}

	@Override
	public void updateIstasyon(Istasyon istasyon) {
		getCurrentSession().update(istasyon);
	}
	
	@Override
	public void deleteIstasyon(Integer id) {
		Istasyon istasyon = getIstasyon(id);
		if (istasyon != null)
			getCurrentSession().delete(istasyon);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Istasyon> getIstasyonList() {
		return getCurrentSession().createCriteria(Istasyon.class).list();
	}
	
}
