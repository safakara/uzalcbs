package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import tr.edu.cu.uzalcbs.model.AnalizDetay;

public interface AnalizDetayDao {
	
	public AnalizDetay getAnalizDetay(Integer id);
	public void addAnalizDetay(AnalizDetay analizDetay);
	public void updateAnalizDetay(AnalizDetay analizDetay);
	public void deleteAnalizDetay(Integer id);
	public List<AnalizDetay> getAnalizDetayList();

}
