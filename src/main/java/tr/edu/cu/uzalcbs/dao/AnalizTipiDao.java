package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import tr.edu.cu.uzalcbs.model.AnalizTipi;

public interface AnalizTipiDao {
	
	public AnalizTipi getAnalizTipi(Integer id);
	public void addAnalizTipi(AnalizTipi analizTipi);
	public void updateAnalizTipi(AnalizTipi analizTipi);
	public void deleteAnalizTipi(Integer id);
	public List<AnalizTipi> getAnalizTipiList();

}
