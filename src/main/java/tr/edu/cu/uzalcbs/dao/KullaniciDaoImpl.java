package tr.edu.cu.uzalcbs.dao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.Kullanici;

@Repository
public class KullaniciDaoImpl implements KullaniciDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public Kullanici getKullanici(Integer id) {
		Kullanici kullanici = (Kullanici) getCurrentSession().get(Kullanici.class, id);
		return kullanici;
	}

	@Override
	public void addKullanici(Kullanici kullanici) {
		kullanici.setOlusturmaTarihi(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		getCurrentSession().save(kullanici);
	}

	@Override
	public void updateKullanici(Kullanici kullanici) {
		kullanici.setOlusturmaTarihi(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		getCurrentSession().update(kullanici);
	}
	
	@Override
	public void deleteKullanici(Integer id) {
		Kullanici kullanici = getKullanici(id);
		if (kullanici != null)
			getCurrentSession().delete(kullanici);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Kullanici> getKullaniciList() {
		return getCurrentSession().createCriteria(Kullanici.class).list();
	}
	
	public Kullanici getKullaniciFindByCriteria(Kullanici kullanici){
		return (Kullanici) getCurrentSession().createCriteria(Kullanici.class)
				.add(Restrictions.eq("mail", kullanici.getMail())).add(Restrictions.eq("sifre", kullanici.getSifre()))
				.uniqueResult();
	}
	
	@Override
	public Kullanici getKullaniciFindByMail(String mail) {
		return (Kullanici) getCurrentSession().createCriteria(Kullanici.class)
				.add(Restrictions.eq("mail", mail))
				.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getKullaniciYetkiList(Kullanici kullanici) {
		StringBuilder sb = new StringBuilder();

		sb.append("select concat_ws('_', s.adi, yt.son_ek) yetkiler \n");
		sb.append("from \n");
		sb.append("    kullanici k, \n");
		sb.append("    kullanici_grup kg, \n");
		sb.append("    sayfa s, \n");
		sb.append("    sayfa_yetkileri sy, \n");
		sb.append("    yetki_tipi yt \n");
		sb.append("where \n");
		sb.append("    k.mail = :mail and k.sifre = :sifre \n");
		sb.append("	and k.kullanici_grup_id = kg.id \n");
		sb.append("	and sy.kullanici_grup_id = kg.id \n");
		sb.append("	and s.id = sy.sayfa_id \n");
		sb.append("	and sy.yetki_tipi_id = yt.id \n");
		
		return (List<String>) getCurrentSession().createSQLQuery(sb.toString())
				.setParameter("mail", kullanici.getMail())
				.setParameter("sifre", kullanici.getSifre())
				.setReadOnly(true).list();
		
	}
	
}
