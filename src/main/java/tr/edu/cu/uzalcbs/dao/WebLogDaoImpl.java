package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.WebLog;

@Repository
public class WebLogDaoImpl implements WebLogDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addWebLog(WebLog webLog) {
		getCurrentSession().save(webLog);
		
	}

	@Override
	public void updateWebLog(WebLog webLog) {
		getCurrentSession().update(webLog); 
	}

	@Override
	public void deleteWebLog(Long id) {
		WebLog webLog = getWebLog(id);
		if(webLog != null){
			getCurrentSession().delete(webLog); 
		}
		
	}

	@Override
	public WebLog getWebLog(Long id) {
		WebLog webLog = (WebLog) getCurrentSession().get(WebLog.class, id);
		return webLog;
	}

	@Override
	public List<WebLog> getWebLogList() {
		return getCurrentSession().createCriteria(WebLog.class).list();
	}

}
