package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import tr.edu.cu.uzalcbs.model.WebLog;

public interface WebLogDao {

	public void addWebLog(WebLog webLog);
	public void updateWebLog(WebLog webLog);
	public void deleteWebLog(Long id);
	public WebLog getWebLog(Long id);
	public List<WebLog> getWebLogList();
}
