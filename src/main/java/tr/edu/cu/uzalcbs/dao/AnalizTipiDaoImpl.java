package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.AnalizTipi;

@Repository
public class AnalizTipiDaoImpl implements AnalizTipiDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public AnalizTipi getAnalizTipi(Integer id) {
		AnalizTipi analizTipi = (AnalizTipi) getCurrentSession().get(AnalizTipi.class, id);
		return analizTipi;
	}

	@Override
	public void addAnalizTipi(AnalizTipi analizTipi) {
		getCurrentSession().save(analizTipi);
	}

	@Override
	public void updateAnalizTipi(AnalizTipi analizTipi) {
		getCurrentSession().update(analizTipi);
	}
	
	@Override
	public void deleteAnalizTipi(Integer id) {
		AnalizTipi analizTipi = getAnalizTipi(id);
		if (analizTipi != null)
			getCurrentSession().delete(analizTipi);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AnalizTipi> getAnalizTipiList() {
		return getCurrentSession().createCriteria(AnalizTipi.class).list();
	}
	
}
