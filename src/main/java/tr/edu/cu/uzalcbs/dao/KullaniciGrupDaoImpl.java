package tr.edu.cu.uzalcbs.dao;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.KullaniciGrup;

@Repository
public class KullaniciGrupDaoImpl implements KullaniciGrupDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public KullaniciGrup getKullaniciGrup(Integer id) {
		KullaniciGrup kullaniciGrup = (KullaniciGrup) getCurrentSession().get(KullaniciGrup.class, id);
		return kullaniciGrup;
	}

	@Override
	public void addKullaniciGrup(KullaniciGrup kullaniciGrup) {
		kullaniciGrup.setOlusturmaTarihi(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		getCurrentSession().save(kullaniciGrup);
	}

	@Override
	public void updateKullaniciGrup(KullaniciGrup kullaniciGrup) {
		kullaniciGrup.setOlusturmaTarihi(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		getCurrentSession().update(kullaniciGrup);
	}
	
	@Override
	public void deleteKullaniciGrup(Integer id) {
		KullaniciGrup kullaniciGrup = getKullaniciGrup(id);
		if (kullaniciGrup != null)
			getCurrentSession().delete(kullaniciGrup);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<KullaniciGrup> getKullaniciGrupList() {
		return getCurrentSession().createCriteria(KullaniciGrup.class).list();
	}
	
}
