package tr.edu.cu.uzalcbs.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.config.AliasToEntityOrderedMapResultTransformer;
import tr.edu.cu.uzalcbs.model.DinamikRapor;

@Repository
public class DinamikRaporDaoImpl implements DinamikRaporDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}


	@Override
	public void addDinamikRapor(DinamikRapor dinamikRapor) {
		getCurrentSession().save(dinamikRapor);
	}

	@Override
	public void updateDinamikRapor(DinamikRapor dinamikRapor) {
		getCurrentSession().update(dinamikRapor);
	}

	@Override
	public DinamikRapor getDinamikRapor(Long id) {
		DinamikRapor dinamikRapor = (DinamikRapor) getCurrentSession().get(DinamikRapor.class, id);
		return dinamikRapor;
	}

	@Override
	public void deleteDinamikRapor(Long id) {
		DinamikRapor dinamikRapor = getDinamikRapor(id);
		if (dinamikRapor != null)
			getCurrentSession().delete(dinamikRapor);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DinamikRapor> getDinamikRaporList() {
		return getCurrentSession().createCriteria(DinamikRapor.class).list();
	}


	@Override
	public List<DinamikRapor> getAktifDinamikRaporList() {
		List<DinamikRapor> list = getCurrentSession().createCriteria(DinamikRapor.class).add(Restrictions.eq("durum", true)).list();
		return list;
	}


	@Override
	public List<DinamikRapor> getAktifDinamikRaporListFindByKullaniciGrup(Integer kullaniciGrupId) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM dinamik_rapor a where a.durum = 1 and ',' || a.yetki || ',' like '%,");
		sb.append(kullaniciGrupId.toString());
		sb.append(",%'");
		SQLQuery query = getCurrentSession().createSQLQuery(sb.toString()).addEntity(DinamikRapor.class);
		return query.list();
	}
	
	@Override
	public List<DinamikRapor> getAktifDinamikRaporListFindByKullaniciGrupAndID(Integer kullaniciGrupId, Long id) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM dinamik_rapor a where a.durum = 1 and a.id = ");
		sb.append(id);
		sb.append(" and regexp_like(a.yetki, '^");
		sb.append(kullaniciGrupId.toString());
		sb.append("$')");
		SQLQuery query = getCurrentSession().createSQLQuery(sb.toString()).addEntity(DinamikRapor.class);
		return query.list();
	}
	
	@Override
	public List getDynamicReport(String sql, HashMap<String, Object> parameters) {
		SQLQuery query = (SQLQuery) getCurrentSession().createSQLQuery(sql);
		query.setResultTransformer(AliasToEntityOrderedMapResultTransformer.INSTANCE);
		
		for(Entry<String, Object> e : parameters.entrySet()) {
	        query.setParameter(e.getKey(), e.getValue());
	    }
		
		return query.list();
	}
	
}
