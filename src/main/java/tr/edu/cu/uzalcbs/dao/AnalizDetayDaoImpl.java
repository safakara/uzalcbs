package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.AnalizDetay;

@Repository
public class AnalizDetayDaoImpl implements AnalizDetayDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public AnalizDetay getAnalizDetay(Integer id) {
		AnalizDetay analizDetay = (AnalizDetay) getCurrentSession().get(AnalizDetay.class, id);
		return analizDetay;
	}

	@Override
	public void addAnalizDetay(AnalizDetay analizDetay) {
		getCurrentSession().save(analizDetay);
	}

	@Override
	public void updateAnalizDetay(AnalizDetay analizDetay) {
		getCurrentSession().update(analizDetay);
	}
	
	@Override
	public void deleteAnalizDetay(Integer id) {
		AnalizDetay analizDetay = getAnalizDetay(id);
		if (analizDetay != null)
			getCurrentSession().delete(analizDetay);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AnalizDetay> getAnalizDetayList() {
		return getCurrentSession().createCriteria(AnalizDetay.class).list();
	}
	
}
