package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import tr.edu.cu.uzalcbs.model.KullaniciGrup;

public interface KullaniciGrupDao {
	
	public KullaniciGrup getKullaniciGrup(Integer id);
	public void addKullaniciGrup(KullaniciGrup kullaniciGrup);
	public void updateKullaniciGrup(KullaniciGrup kullaniciGrup);
	public void deleteKullaniciGrup(Integer id);
	public List<KullaniciGrup> getKullaniciGrupList();

}
