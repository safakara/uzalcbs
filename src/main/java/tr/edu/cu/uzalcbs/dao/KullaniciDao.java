package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import tr.edu.cu.uzalcbs.model.Kullanici;

public interface KullaniciDao {
	
	public Kullanici getKullanici(Integer id);
	public void addKullanici(Kullanici kullanici);
	public void updateKullanici(Kullanici kullanici);
	public void deleteKullanici(Integer id);
	public List<Kullanici> getKullaniciList();
	public Kullanici getKullaniciFindByCriteria(Kullanici kullanici);
	public Kullanici getKullaniciFindByMail(String mail);
	public List<String> getKullaniciYetkiList(Kullanici kullanici);

}
