package tr.edu.cu.uzalcbs.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.edu.cu.uzalcbs.model.Sayfa;

@Repository
public class SayfaDaoImpl implements SayfaDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public Sayfa getSayfa(Integer id) {
		Sayfa sayfa = (Sayfa) getCurrentSession().get(Sayfa.class, id);
		return sayfa;
	}

	@Override
	public void addSayfa(Sayfa sayfa) {
		getCurrentSession().save(sayfa);
	}

	@Override
	public void updateSayfa(Sayfa sayfa) {
		getCurrentSession().update(sayfa);
	}
	
	@Override
	public void deleteSayfa(Integer id) {
		Sayfa sayfa = getSayfa(id);
		if (sayfa != null)
			getCurrentSession().delete(sayfa);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Sayfa> getSayfaList() {
		return getCurrentSession().createCriteria(Sayfa.class).list();
	}
	
}
