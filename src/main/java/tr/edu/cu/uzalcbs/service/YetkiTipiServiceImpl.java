package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.YetkiTipiDao;
import tr.edu.cu.uzalcbs.model.YetkiTipi;

@Service
@Transactional
public class YetkiTipiServiceImpl implements YetkiTipiService{

	@Autowired
	private YetkiTipiDao yetkiTipiDao;

	@Override
	public YetkiTipi getYetkiTipi(Integer id) {
		return yetkiTipiDao.getYetkiTipi(id);
	}

	@Override
	public void addYetkiTipi(YetkiTipi yetkiTipi) {
		yetkiTipiDao.addYetkiTipi(yetkiTipi);
	}

	@Override
	public void updateYetkiTipi(YetkiTipi yetkiTipi) {
		yetkiTipiDao.updateYetkiTipi(yetkiTipi);
	}

	@Override
	public void deleteYetkiTipi(Integer id) {
		yetkiTipiDao.deleteYetkiTipi(id);
	}

	@Override
	public List<YetkiTipi> getYetkiTipiList() {
		return yetkiTipiDao.getYetkiTipiList();
	}
	
}
