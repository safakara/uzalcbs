package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.SayfaDao;
import tr.edu.cu.uzalcbs.model.Sayfa;

@Service
@Transactional
public class SayfaServiceImpl implements SayfaService{

	@Autowired
	private SayfaDao sayfaDao;

	@Override
	public Sayfa getSayfa(Integer id) {
		return sayfaDao.getSayfa(id);
	}

	@Override
	public void addSayfa(Sayfa sayfa) {
		sayfaDao.addSayfa(sayfa);
	}

	@Override
	public void updateSayfa(Sayfa sayfa) {
		sayfaDao.updateSayfa(sayfa);
	}

	@Override
	public void deleteSayfa(Integer id) {
		sayfaDao.deleteSayfa(id);
	}

	@Override
	public List<Sayfa> getSayfaList() {
		return sayfaDao.getSayfaList();
	}
	
}
