package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.AnalizDetayDao;
import tr.edu.cu.uzalcbs.model.AnalizDetay;

@Service
@Transactional
public class AnalizDetayServiceImpl implements AnalizDetayService{

	@Autowired
	private AnalizDetayDao analizDetayDao;

	@Override
	public AnalizDetay getAnalizDetay(Integer id) {
		return analizDetayDao.getAnalizDetay(id);
	}

	@Override
	public void addAnalizDetay(AnalizDetay analizDetay) {
		analizDetayDao.addAnalizDetay(analizDetay);
	}

	@Override
	public void updateAnalizDetay(AnalizDetay analizDetay) {
		analizDetayDao.updateAnalizDetay(analizDetay);
	}

	@Override
	public void deleteAnalizDetay(Integer id) {
		analizDetayDao.deleteAnalizDetay(id);
	}

	@Override
	public List<AnalizDetay> getAnalizDetayList() {
		return analizDetayDao.getAnalizDetayList();
	}
	
}
