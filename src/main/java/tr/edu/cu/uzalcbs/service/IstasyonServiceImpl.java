package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.IstasyonDao;
import tr.edu.cu.uzalcbs.model.Istasyon;

@Service
@Transactional
public class IstasyonServiceImpl implements IstasyonService{

	@Autowired
	private IstasyonDao istasyonDao;

	@Override
	public Istasyon getIstasyon(Integer id) {
		return istasyonDao.getIstasyon(id);
	}

	@Override
	public void addIstasyon(Istasyon istasyon) {
		istasyonDao.addIstasyon(istasyon);
	}

	@Override
	public void updateIstasyon(Istasyon istasyon) {
		istasyonDao.updateIstasyon(istasyon);
	}

	@Override
	public void deleteIstasyon(Integer id) {
		istasyonDao.deleteIstasyon(id);
	}

	@Override
	public List<Istasyon> getIstasyonList() {
		return istasyonDao.getIstasyonList();
	}
	
}
