package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.KullaniciGrupDao;
import tr.edu.cu.uzalcbs.model.KullaniciGrup;

@Service
@Transactional
public class KullaniciGrupServiceImpl implements KullaniciGrupService{

	@Autowired
	private KullaniciGrupDao kullaniciGrupDao;

	@Override
	public KullaniciGrup getKullaniciGrup(Integer id) {
		return kullaniciGrupDao.getKullaniciGrup(id);
	}

	@Override
	public void addKullaniciGrup(KullaniciGrup kullaniciGrup) {
		kullaniciGrupDao.addKullaniciGrup(kullaniciGrup);
	}

	@Override
	public void updateKullaniciGrup(KullaniciGrup kullaniciGrup) {
		kullaniciGrupDao.updateKullaniciGrup(kullaniciGrup);
	}

	@Override
	public void deleteKullaniciGrup(Integer id) {
		kullaniciGrupDao.deleteKullaniciGrup(id);
	}

	@Override
	public List<KullaniciGrup> getKullaniciGrupList() {
		return kullaniciGrupDao.getKullaniciGrupList();
	}
	
}
