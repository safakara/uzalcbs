package tr.edu.cu.uzalcbs.service;

import java.util.List;

import tr.edu.cu.uzalcbs.model.AnalizTipi;

public interface AnalizTipiService {

	public AnalizTipi getAnalizTipi(Integer id);
	public void addAnalizTipi(AnalizTipi analizTipi);
	public void updateAnalizTipi(AnalizTipi analizTipi);
	public void deleteAnalizTipi(Integer id);
	public List<AnalizTipi> getAnalizTipiList();

}
