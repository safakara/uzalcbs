package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.AnalizTipiDao;
import tr.edu.cu.uzalcbs.model.AnalizTipi;

@Service
@Transactional
public class AnalizTipiServiceImpl implements AnalizTipiService{

	@Autowired
	private AnalizTipiDao analizTipiDao;

	@Override
	public AnalizTipi getAnalizTipi(Integer id) {
		return analizTipiDao.getAnalizTipi(id);
	}

	@Override
	public void addAnalizTipi(AnalizTipi analizTipi) {
		analizTipiDao.addAnalizTipi(analizTipi);
	}

	@Override
	public void updateAnalizTipi(AnalizTipi analizTipi) {
		analizTipiDao.updateAnalizTipi(analizTipi);
	}

	@Override
	public void deleteAnalizTipi(Integer id) {
		analizTipiDao.deleteAnalizTipi(id);
	}

	@Override
	public List<AnalizTipi> getAnalizTipiList() {
		return analizTipiDao.getAnalizTipiList();
	}
	
}
