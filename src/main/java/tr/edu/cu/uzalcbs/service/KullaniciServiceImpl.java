package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.KullaniciDao;
import tr.edu.cu.uzalcbs.model.Kullanici;

@Service
@Transactional
public class KullaniciServiceImpl implements KullaniciService{

	@Autowired
	private KullaniciDao kullaniciDao;

	@Override
	public Kullanici getKullanici(Integer id) {
		return kullaniciDao.getKullanici(id);
	}

	@Override
	public void addKullanici(Kullanici kullanici) {
		kullaniciDao.addKullanici(kullanici);
	}

	@Override
	public void updateKullanici(Kullanici kullanici) {
		kullaniciDao.updateKullanici(kullanici);
	}

	@Override
	public void deleteKullanici(Integer id) {
		kullaniciDao.deleteKullanici(id);
	}

	@Override
	public List<Kullanici> getKullaniciList() {
		return kullaniciDao.getKullaniciList();
	}

	@Override
	public Kullanici getKullaniciFindByCriteria(Kullanici kullanici) {
		return kullaniciDao.getKullaniciFindByCriteria(kullanici);
	}

	@Override
	public Kullanici getKullaniciFindByMail(String mail) {
		return kullaniciDao.getKullaniciFindByMail(mail);
	}

	@Override
	public List<String> getKullaniciYetkiList(Kullanici kullanici) {
		return kullaniciDao.getKullaniciYetkiList(kullanici);
	}
	
}
