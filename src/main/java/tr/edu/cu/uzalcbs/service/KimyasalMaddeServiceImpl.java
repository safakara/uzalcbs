package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.KimyasalMaddeDao;
import tr.edu.cu.uzalcbs.model.KimyasalMadde;

@Service
@Transactional
public class KimyasalMaddeServiceImpl implements KimyasalMaddeService{

	@Autowired
	private KimyasalMaddeDao kimyasalMaddeDao;

	@Override
	public KimyasalMadde getKimyasalMadde(Integer id) {
		return kimyasalMaddeDao.getKimyasalMadde(id);
	}

	@Override
	public void addKimyasalMadde(KimyasalMadde kimyasalMadde) {
		kimyasalMaddeDao.addKimyasalMadde(kimyasalMadde);
	}

	@Override
	public void updateKimyasalMadde(KimyasalMadde kimyasalMadde) {
		kimyasalMaddeDao.updateKimyasalMadde(kimyasalMadde);
	}

	@Override
	public void deleteKimyasalMadde(Integer id) {
		kimyasalMaddeDao.deleteKimyasalMadde(id);
	}

	@Override
	public List<KimyasalMadde> getKimyasalMaddeList() {
		return kimyasalMaddeDao.getKimyasalMaddeList();
	}
	
}
