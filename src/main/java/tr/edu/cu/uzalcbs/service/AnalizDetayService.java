package tr.edu.cu.uzalcbs.service;

import java.util.List;

import tr.edu.cu.uzalcbs.model.AnalizDetay;

public interface AnalizDetayService {

	public AnalizDetay getAnalizDetay(Integer id);
	public void addAnalizDetay(AnalizDetay analizDetay);
	public void updateAnalizDetay(AnalizDetay analizDetay);
	public void deleteAnalizDetay(Integer id);
	public List<AnalizDetay> getAnalizDetayList();

}
