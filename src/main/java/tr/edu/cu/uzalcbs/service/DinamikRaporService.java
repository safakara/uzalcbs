package tr.edu.cu.uzalcbs.service;

import java.util.HashMap;
import java.util.List;

import tr.edu.cu.uzalcbs.model.DinamikRapor;

public interface DinamikRaporService {
	
	public void addDinamikRapor(DinamikRapor dinamikRapor);
	public void updateDinamikRapor(DinamikRapor dinamikRapor);
	public DinamikRapor getDinamikRapor(Long id);
	public void deleteDinamikRapor(Long id);
	public List<DinamikRapor> getDinamikRaporList();
	public List<DinamikRapor> getAktifDinamikRaporList();
	public List<DinamikRapor> getAktifDinamikRaporListFindByKullaniciGrup(Integer kullaniciGrupId);
	public List getDynamicReport(String sql, HashMap<String, Object> parameters);
	public List<DinamikRapor> getAktifDinamikRaporListFindByKullaniciGrupAndID(Integer kullaniciGrupId, Long id);

}
