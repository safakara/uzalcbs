package tr.edu.cu.uzalcbs.service;

import java.util.List;

import tr.edu.cu.uzalcbs.model.Istasyon;

public interface IstasyonService {

	public Istasyon getIstasyon(Integer id);
	public void addIstasyon(Istasyon istasyon);
	public void updateIstasyon(Istasyon istasyon);
	public void deleteIstasyon(Integer id);
	public List<Istasyon> getIstasyonList();

}
