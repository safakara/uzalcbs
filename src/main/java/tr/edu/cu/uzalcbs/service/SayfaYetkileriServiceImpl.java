package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.SayfaYetkileriDao;
import tr.edu.cu.uzalcbs.model.SayfaYetkileri;

@Service
@Transactional
public class SayfaYetkileriServiceImpl implements SayfaYetkileriService{

	@Autowired
	private SayfaYetkileriDao sayfaYetkileriDao;

	@Override
	public SayfaYetkileri getSayfaYetkileri(Integer id) {
		return sayfaYetkileriDao.getSayfaYetkileri(id);
	}

	@Override
	public void addSayfaYetkileri(SayfaYetkileri sayfaYetkileri) {
		sayfaYetkileriDao.addSayfaYetkileri(sayfaYetkileri);
	}

	@Override
	public void updateSayfaYetkileri(SayfaYetkileri sayfaYetkileri) {
		sayfaYetkileriDao.updateSayfaYetkileri(sayfaYetkileri);
	}

	@Override
	public void deleteSayfaYetkileri(Integer id) {
		sayfaYetkileriDao.deleteSayfaYetkileri(id);
	}

	@Override
	public List<SayfaYetkileri> getSayfaYetkileriList() {
		return sayfaYetkileriDao.getSayfaYetkileriList();
	}
	
}
