package tr.edu.cu.uzalcbs.service;

import java.util.List;

import tr.edu.cu.uzalcbs.model.Sayfa;

public interface SayfaService {

	public Sayfa getSayfa(Integer id);
	public void addSayfa(Sayfa sayfa);
	public void updateSayfa(Sayfa sayfa);
	public void deleteSayfa(Integer id);
	public List<Sayfa> getSayfaList();

}
