package tr.edu.cu.uzalcbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.WebLogDao;
import tr.edu.cu.uzalcbs.dao.YetkiTipiDao;
import tr.edu.cu.uzalcbs.model.WebLog;
import tr.edu.cu.uzalcbs.model.YetkiTipi;

@Service
@Transactional
public class WebLogServiceImpl implements WebLogService{

	@Autowired
	private WebLogDao webLogDao;

	@Override
	public void addWebLog(WebLog webLog) {
		webLogDao.addWebLog(webLog);
	}

	@Override
	public void updateWebLog(WebLog webLog) {
		webLogDao.updateWebLog(webLog);
	}

	@Override
	public void deleteWebLog(Long id) {
		webLogDao.deleteWebLog(id);
	}

	@Override
	public WebLog getWebLog(Long id) {
		return webLogDao.getWebLog(id);
	}

	@Override
	public List<WebLog> getWebLogList() {
		return webLogDao.getWebLogList();
	}

}
