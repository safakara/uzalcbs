package tr.edu.cu.uzalcbs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tr.edu.cu.uzalcbs.dao.DinamikRaporDao;
import tr.edu.cu.uzalcbs.model.DinamikRapor;

@Service
@Transactional
public class DinamikRaporServiceImpl implements DinamikRaporService {
	
	@Autowired
	private DinamikRaporDao dinamikRaporDao;

	@Override
	public void addDinamikRapor(DinamikRapor dinamikRapor) {
		dinamikRaporDao.addDinamikRapor(dinamikRapor);
	}

	@Override
	public void updateDinamikRapor(DinamikRapor dinamikRapor) {
		dinamikRaporDao.updateDinamikRapor(dinamikRapor);		
	}

	@Override
	public DinamikRapor getDinamikRapor(Long id) {
		return dinamikRaporDao.getDinamikRapor(id);
	}

	@Override
	public void deleteDinamikRapor(Long id) {
		dinamikRaporDao.deleteDinamikRapor(id);
	}

	@Override
	public List<DinamikRapor> getDinamikRaporList() {
		return dinamikRaporDao.getDinamikRaporList();
	}

	@Override
	public List<DinamikRapor> getAktifDinamikRaporList() {
		return dinamikRaporDao.getAktifDinamikRaporList();
	}

	@Override
	public List<DinamikRapor> getAktifDinamikRaporListFindByKullaniciGrup(Integer kullaniciGrupId) {
		return dinamikRaporDao.getAktifDinamikRaporListFindByKullaniciGrup(kullaniciGrupId);
	}
	
	@Override
	public List<DinamikRapor> getAktifDinamikRaporListFindByKullaniciGrupAndID(Integer kullaniciGrupId, Long id) {
		return dinamikRaporDao.getAktifDinamikRaporListFindByKullaniciGrupAndID(kullaniciGrupId, id);
	}
	
	@Override
	public List getDynamicReport(String sql, HashMap<String, Object> parameters) {
		return dinamikRaporDao.getDynamicReport(sql, parameters);
	}

}
