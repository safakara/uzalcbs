package tr.edu.cu.uzalcbs.service;

import java.util.List;

import tr.edu.cu.uzalcbs.model.SayfaYetkileri;

public interface SayfaYetkileriService {

	public SayfaYetkileri getSayfaYetkileri(Integer id);
	public void addSayfaYetkileri(SayfaYetkileri sayfaYetkileri);
	public void updateSayfaYetkileri(SayfaYetkileri sayfaYetkileri);
	public void deleteSayfaYetkileri(Integer id);
	public List<SayfaYetkileri> getSayfaYetkileriList();

}
