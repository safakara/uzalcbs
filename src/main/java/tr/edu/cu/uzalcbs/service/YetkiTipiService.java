package tr.edu.cu.uzalcbs.service;

import java.util.List;

import tr.edu.cu.uzalcbs.model.YetkiTipi;

public interface YetkiTipiService {

	public YetkiTipi getYetkiTipi(Integer id);
	public void addYetkiTipi(YetkiTipi yetkiTipi);
	public void updateYetkiTipi(YetkiTipi yetkiTipi);
	public void deleteYetkiTipi(Integer id);
	public List<YetkiTipi> getYetkiTipiList();

}
