package tr.edu.cu.uzalcbs.service;

import java.util.List;

import tr.edu.cu.uzalcbs.model.KullaniciGrup;

public interface KullaniciGrupService {

	public KullaniciGrup getKullaniciGrup(Integer id);
	public void addKullaniciGrup(KullaniciGrup kullaniciGrup);
	public void updateKullaniciGrup(KullaniciGrup kullaniciGrup);
	public void deleteKullaniciGrup(Integer id);
	public List<KullaniciGrup> getKullaniciGrupList();

}
