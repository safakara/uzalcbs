package tr.edu.cu.uzalcbs.form;

import java.util.HashMap;


public class DinamikRaporFormInputs {
	
	//[{"_name":"ucretBandi","_type":"text","_class":"required","_width":"4","_title":"Ücret Bandı","_value":"1:Aylık|2:Günlük","_api":"","_apiKey":"","_apiVal":""}]

	private String _name;
	private String _type;
	private String _class;
	private Integer _width;
	private String _title;
	private String _valueStr;
	private HashMap<String, String> _valueArr;
	private String _api;
	private String _apiKey;
	private String _apiVal;
	
	public String get_name() {
		return _name;
	}
	public void set_name(String _name) {
		this._name = _name;
	}
	public String get_type() {
		return _type;
	}
	public void set_type(String _type) {
		this._type = _type;
	}
	public String get_class() {
		return _class;
	}
	public void set_class(String _class) {
		this._class = _class;
	}
	public Integer get_width() {
		return _width;
	}
	public void set_width(Integer _width) {
		this._width = _width;
	}
	public String get_title() {
		return _title;
	}
	public void set_title(String _title) {
		this._title = _title;
	}
	public String get_valueStr() {
		return _valueStr;
	}
	public void set_valueStr(String _valueStr) {
		this._valueStr = _valueStr;
	}
	public HashMap<String, String> get_valueArr() {
		return _valueArr;
	}
	public void set_valueArr(HashMap<String, String> _valueArr) {
		this._valueArr = _valueArr;
	}
	public String get_api() {
		return _api;
	}
	public void set_api(String _api) {
		this._api = _api;
	}
	public String get_apiKey() {
		return _apiKey;
	}
	public void set_apiKey(String _apiKey) {
		this._apiKey = _apiKey;
	}
	public String get_apiVal() {
		return _apiVal;
	}
	public void set_apiVal(String _apiVal) {
		this._apiVal = _apiVal;
	}

}
