package tr.edu.cu.uzalcbs.controller;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class GlobalExceptionController {

	@ExceptionHandler(NoHandlerFoundException.class)
	public ModelAndView handleError404(HttpServletRequest request, Exception e) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("errCode", "Bilinmeyen Url !!");
		mav.addObject("errMsg", e.getMessage() + "<br/>" + e.getCause().getMessage());
		mav.setViewName("error_page");
		return mav;
	}

	@ExceptionHandler(value = Exception.class)
	public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		// If the exception is annotated with @ResponseStatus rethrow it and let
		// the framework handle it - like the OrderNotFoundException example
		// at the start of this post.
		// AnnotationUtils is a Spring Framework utility class.
		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
			throw e;

		// Otherwise setup and send the user to a default error-view.
		ModelAndView mav = new ModelAndView();
		mav.addObject("errCode", "Hata !!");
		mav.addObject("errMsg", e.getMessage());
		mav.setViewName("error_page");
		return mav;
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ModelAndView conflict(DataIntegrityViolationException ex) {

		// extract the affected database constraint name:
		String constraintName = null;
		ModelAndView model = new ModelAndView("error_page_constraint_violation");

		if ((ex.getCause() != null) && (ex.getCause() instanceof ConstraintViolationException)) {

			constraintName = ((ConstraintViolationException) ex.getCause()).getConstraintName();
			model.addObject("errCode", "Hata !!");
			model.addObject("errMsg", constraintName
					+ " tekil kural ihlali gerçekleşti. Tekil alan (Id, Primary Key vb) için aynı değerde kayıt bulunmaktadır. Lütfen başka bir değer girerek, tekrar deneyiniz.");
			model.addObject("errDetail", ex.getRootCause());

		}

		return model;
	}

	/*
	 * @ExceptionHandler(Exception.class) public ModelAndView
	 * handleAllException(Exception ex) {
	 * 
	 * ModelAndView model = new ModelAndView("error_page");
	 * model.addObject("errCode", "Hata"); model.addObject("errMsg",
	 * "İşlem Gerçekleştirelemedi!"); return model; }
	 */


}
