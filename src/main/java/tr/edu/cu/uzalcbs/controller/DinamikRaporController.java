package tr.edu.cu.uzalcbs.controller;

import java.security.Principal;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tr.edu.cu.uzalcbs.config.Util;
import tr.edu.cu.uzalcbs.model.DinamikRapor;
import tr.edu.cu.uzalcbs.service.DinamikRaporService;

@Controller
@RequestMapping(value = "/admin/dinamik-rapor")
public class DinamikRaporController {

	@Autowired
	private DinamikRaporService dinamikRaporService;
	
	@Autowired
	private Util util;
	
	@Secured({"ROLE_ADMIN", "PERM_DINAMIK_RAPOR_ISLEMLERI_OKUMA"})
	@RequestMapping(value = "/olustur", method = RequestMethod.GET)
	public ModelAndView addDinamikRaporPage() {
		ModelAndView modelAndView = new ModelAndView("PAGE_dinamikRapor");
		List<DinamikRapor> dinamikRaporList = dinamikRaporService.getDinamikRaporList();
		modelAndView.addObject("dinamikRapor", new DinamikRapor());
		modelAndView.addObject("dinamikRaporList", dinamikRaporList);
		return modelAndView;
	}
	
	@Secured({"ROLE_ADMIN", "PERM_DINAMIK_RAPOR_ISLEMLERI_YAZMA"})
	@RequestMapping(value = "/olustur", method = RequestMethod.POST)
	public ModelAndView addingDinamikRaporPage(@ModelAttribute DinamikRapor dinamikRapor) {
		ModelAndView modelAndView = new ModelAndView("PAGE_dinamikRapor");
		
		dinamikRapor.setKullanici(util.getKullaniciAdi());
		dinamikRapor.setTarih(Calendar.getInstance().getTime());
		
		dinamikRaporService.addDinamikRapor(dinamikRapor);
		List<DinamikRapor> dinamikRaporList = dinamikRaporService.getDinamikRaporList();
		modelAndView.addObject("dinamikRaporList", dinamikRaporList);
		modelAndView.addObject("msg", "Dinamik rapor ekleme işlemi başarıyla gerçekleştirilmiştir.");
		return modelAndView;
	}

	@Secured({"ROLE_ADMIN", "PERM_DINAMIK_RAPOR_ISLEMLERI_YAZMA"})
	@RequestMapping(value = "/guncelle/{id}", method = RequestMethod.GET)
	public ModelAndView editDinamikRaporPage(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView("PAGE_dinamikRapor");
		DinamikRapor dinamikRapor = dinamikRaporService.getDinamikRapor(id);
		List<DinamikRapor> dinamikRaporList = dinamikRaporService.getDinamikRaporList();
		modelAndView.addObject("dinamikRapor", dinamikRapor);
		modelAndView.addObject("dinamikRaporList", dinamikRaporList);
		return modelAndView;
	}

	@Secured({"ROLE_ADMIN", "PERM_DINAMIK_RAPOR_ISLEMLERI_YAZMA"})
	@RequestMapping(value = "/guncelle/{id}", method = RequestMethod.POST)
	public ModelAndView edditingDinamikRapor(@ModelAttribute DinamikRapor dinamikRapor, @PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView("PAGE_dinamikRapor");
		
		dinamikRapor.setKullanici(util.getKullaniciAdi());
		dinamikRapor.setTarih(Calendar.getInstance().getTime());
		
		dinamikRaporService.updateDinamikRapor(dinamikRapor);
		List<DinamikRapor> dinamikRaporList = dinamikRaporService.getDinamikRaporList();
		modelAndView.addObject("dinamikRapor", dinamikRapor);
		modelAndView.addObject("dinamikRaporList", dinamikRaporList);
		modelAndView.addObject("msg", "Dinamik rapor güncelleme işlemi başarıyla gerçekleştirilmiştir.");
		return modelAndView;
	}

	@Secured({"ROLE_ADMIN", "PERM_DINAMIK_RAPOR_ISLEMLERI_SILME"})
	@RequestMapping(value = "/sil/{id}", method = RequestMethod.GET)
	public String deleteDinamikRapor(@PathVariable Long id, final RedirectAttributes redirectAttributes) {
		dinamikRaporService.deleteDinamikRapor(id);
		redirectAttributes.addFlashAttribute("msg", "Dinamik rapor silme işlemi başarıyla gerçekleştirilmiştir.");
		return "redirect:/admin/dinamik-rapor/olustur";
	}

}
