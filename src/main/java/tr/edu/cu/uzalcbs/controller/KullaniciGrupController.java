package tr.edu.cu.uzalcbs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tr.edu.cu.uzalcbs.model.KullaniciGrup;
import tr.edu.cu.uzalcbs.service.KullaniciGrupService;

@Controller
@RequestMapping(value = "/admin/kullanici-grup")
public class KullaniciGrupController {

	@Autowired
	private KullaniciGrupService kullaniciGrupService;
	
	final String MSG = "Kullanıcı grup";
	
	@Secured({"ROLE_ADMIN", "KULLANICI_GRUP_OKUMA"})
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView addKullaniciGrupPage() {
		ModelAndView modelAndView = new ModelAndView("PAGE_kullaniciGrup");
		List<KullaniciGrup> kullaniciGrupList = kullaniciGrupService.getKullaniciGrupList();
		modelAndView.addObject("kullaniciGrup", new KullaniciGrup());
		modelAndView.addObject("kullaniciGrupList", kullaniciGrupList);
		return modelAndView;
	}
	
	@Secured({"ROLE_ADMIN", "KULLANICI_GRUP_YAZMA"})
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String addingKullaniciGrupPage(@ModelAttribute KullaniciGrup kullaniciGrup, final RedirectAttributes redirectAttributes) {
		kullaniciGrupService.addKullaniciGrup(kullaniciGrup);
		redirectAttributes.addFlashAttribute("msg", MSG.concat(" ekleme işlemi başarıyla gerçekleştirilmiştir."));
		return "redirect:/admin/kullanici-grup/edit/" + kullaniciGrup.getId();
	}

	@Secured({"ROLE_ADMIN", "KULLANICI_GRUP_YAZMA"})
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editKullaniciGrupPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("PAGE_kullaniciGrup");
		KullaniciGrup kullaniciGrup = kullaniciGrupService.getKullaniciGrup(id);
		List<KullaniciGrup> kullaniciGrupList = kullaniciGrupService.getKullaniciGrupList();
		modelAndView.addObject("kullaniciGrup", kullaniciGrup);
		modelAndView.addObject("kullaniciGrupList", kullaniciGrupList);
		return modelAndView;
	}

	@Secured({"ROLE_ADMIN", "KULLANICI_GRUP_YAZMA"})
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editingKullaniciGrup(@ModelAttribute KullaniciGrup kullaniciGrup, @PathVariable Integer id, final RedirectAttributes redirectAttributes) {
		kullaniciGrupService.updateKullaniciGrup(kullaniciGrup);
		redirectAttributes.addFlashAttribute("msg", MSG.concat(" güncelleme işlemi başarıyla gerçekleştirilmiştir."));
		return "redirect:/admin/kullanici-grup/edit/" + kullaniciGrup.getId();
	}

	@Secured({"ROLE_ADMIN", "KULLANICI_GRUP_SILME"})
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteKullaniciGrup(@PathVariable Integer id, final RedirectAttributes redirectAttributes) {
		kullaniciGrupService.deleteKullaniciGrup(id);
		redirectAttributes.addFlashAttribute("msg", MSG.concat(" silme işlemi başarıyla gerçekleştirilmiştir."));
		return "redirect:/admin/kullanici-grup/";
	}
	

}
