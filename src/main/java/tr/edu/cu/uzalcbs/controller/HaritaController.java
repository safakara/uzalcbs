package tr.edu.cu.uzalcbs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/admin/harita")
public class HaritaController {

	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView addKullaniciGrupPage() {
		ModelAndView modelAndView = new ModelAndView("PAGE_harita");
		
		return modelAndView;
	}
	
	

}
