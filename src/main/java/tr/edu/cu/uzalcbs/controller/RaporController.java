package tr.edu.cu.uzalcbs.controller;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import tr.edu.cu.uzalcbs.builder.DynamicReport;
import tr.edu.cu.uzalcbs.model.DinamikRapor;
import tr.edu.cu.uzalcbs.service.DinamikRaporService;

@Controller
@RequestMapping(value = "/admin/rapor")
public class RaporController {

	@Autowired
	private DinamikRaporService dinamikRaporService;
	
	@Secured({"ROLE_ADMIN", "PERM_RAPORLAMA_OKUMA"})
	@RequestMapping(value = "/{id}")
	public ModelAndView getRapor(@PathVariable Long id, HttpServletRequest request, Authentication auth) {
		
		ModelAndView modelAndView = new ModelAndView();
		boolean state = false;	
		List<DinamikRapor> dinamikRaporList =   (List<DinamikRapor>) request.getSession().getAttribute("dinamikRaporList");
		
		for(DinamikRapor tmp : dinamikRaporList){
			state = tmp.getId().equals(id) ? true : state ;
		}
		
		for(GrantedAuthority authorities : (List<GrantedAuthority>) auth.getAuthorities()){
			state = authorities.getAuthority().equals("ROLE_ADMIN") ? true : state ;
		}
		
		
		if(state){
			modelAndView.setViewName("PAGE_rapor");
			DinamikRapor dinamikRapor = dinamikRaporService.getDinamikRapor(id);
			DynamicReport report = new DynamicReport.Builder(dinamikRapor, dinamikRaporService, request).build();
			modelAndView.addObject("inputForm", report.getInputForm());
			modelAndView.addObject("table", report.getTable());
			modelAndView.addObject("rawData", report.getRawData());
			modelAndView.addObject("dinamikRapor", dinamikRapor);
		}else {
			modelAndView.setViewName("403");
		}
		
		return modelAndView;
	}


}
