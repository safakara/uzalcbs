package tr.edu.cu.uzalcbs.controller;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tr.edu.cu.uzalcbs.model.Kullanici;
import tr.edu.cu.uzalcbs.model.KullaniciGrup;
import tr.edu.cu.uzalcbs.service.KullaniciGrupService;
import tr.edu.cu.uzalcbs.service.KullaniciService;

@Controller
@RequestMapping(value = "/admin/kullanici")
public class KullaniciController {

	@Autowired
	private KullaniciService kullaniciService;
	
	@Autowired
	private KullaniciGrupService kullaniciGrupService;
	
	final String MSG = "Kullanıcı";
	
	@Secured({"ROLE_ADMIN", "KULLANICI_OKUMA"})
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView addKullaniciPage() {
		ModelAndView modelAndView = new ModelAndView("PAGE_kullanici");
		List<Kullanici> kullaniciList = kullaniciService.getKullaniciList();
		Kullanici kullanici = new Kullanici();
		kullanici.setKullaniciGrup(new KullaniciGrup());
		modelAndView.addObject("kullanici", kullanici);
		modelAndView.addObject("kullaniciList", kullaniciList);
		return modelAndView;
	}
	
	@Secured({"ROLE_ADMIN", "KULLANICI_YAZMA"})
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String addingKullaniciPage(@ModelAttribute Kullanici kullanici, final RedirectAttributes redirectAttributes) {
		kullanici.setSifre(DigestUtils.md5Hex(kullanici.getSifre()));
 		kullaniciService.addKullanici(kullanici);
		redirectAttributes.addFlashAttribute("msg", MSG.concat(" ekleme işlemi başarıyla gerçekleştirilmiştir."));
		return "redirect:/admin/kullanici/edit/" + kullanici.getId();
	}

	@Secured({"ROLE_ADMIN", "KULLANICI_YAZMA"})
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editKullaniciPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("PAGE_kullanici");
		Kullanici kullanici = kullaniciService.getKullanici(id);
		List<Kullanici> kullaniciList = kullaniciService.getKullaniciList();
		modelAndView.addObject("kullanici", kullanici);
		modelAndView.addObject("kullaniciList", kullaniciList);
		return modelAndView;
	}

	@Secured({"ROLE_ADMIN", "KULLANICI_YAZMA"})
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editingKullanici(@ModelAttribute Kullanici kullanici, @PathVariable Integer id, final RedirectAttributes redirectAttributes) {
		kullanici.setSifre(DigestUtils.md5Hex(kullanici.getSifre()));
		kullaniciService.updateKullanici(kullanici);
		redirectAttributes.addFlashAttribute("msg", MSG.concat(" güncelleme işlemi başarıyla gerçekleştirilmiştir."));
		return "redirect:/admin/kullanici/edit/" + kullanici.getId();
	}

	@Secured({"ROLE_ADMIN", "KULLANICI_SILME"})
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteKullanici(@PathVariable Integer id, final RedirectAttributes redirectAttributes) {
		kullaniciService.deleteKullanici(id);
		redirectAttributes.addFlashAttribute("msg", MSG.concat(" silme işlemi başarıyla gerçekleştirilmiştir."));
		return "redirect:/admin/kullanici/";
	}

}
