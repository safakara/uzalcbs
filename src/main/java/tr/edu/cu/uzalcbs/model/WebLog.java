package tr.edu.cu.uzalcbs.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="WEB_LOG")
public class WebLog {

	@Basic
	@Id
	@GeneratedValue
	@Column(name="ID", unique= true, nullable= false)
	private Long id;
	
	@Column(name="HEADER", nullable= false, length = 4000)
	private String header;
	
	@Column(name="FORM", nullable= false, length = 4000)
	private String form;
	
	@Column(name="SCHEMA", nullable= true, length = 20)
	private String schema;
	
	@Column(name="USERNAME", nullable= true, length = 100)
	private String username;
	
	@Column(name="IP", nullable= true, length = 30)
	private String ip;
	
	@Column(name="PATH", nullable= true, length = 100)
	private String path;
	
	@Column(name="METHOD", nullable= true, length = 10)
	private String method;
	
	@DateTimeFormat(pattern="dd.MM.yyyy HH:mm")
	@Column(name="ISLEM_TARIHI", nullable= false)
	private Date islemTarihi;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Date getIslemTarihi() {
		return islemTarihi;
	}

	public void setIslemTarihi(Date islemTarihi) {
		this.islemTarihi = islemTarihi;
	}
	
}
