package tr.edu.cu.uzalcbs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="WEB_MODULE", uniqueConstraints=@UniqueConstraint(columnNames = {"ID","ADI"}))
public class WebModule {
	
	@Id
	@Column(name="ID", nullable=false)
	private Integer id;
	
	@Column(name="ADI", nullable=false, length=100)
	private String adi;
	
	@Column(name="ON_EK", nullable=false, length=100)
	private String onEk;
	
	@Column(name="BAGLI_ID")
	private Integer bagliId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAdi() {
		return adi;
	}

	public void setAdi(String adi) {
		this.adi = adi;
	}

	public String getOnEk() {
		return onEk;
	}

	public void setOnEk(String onEk) {
		this.onEk = onEk;
	}

	public Integer getBagliId() {
		return bagliId;
	}

	public void setBagliId(Integer bagliId) {
		this.bagliId = bagliId;
	}

	
	
}
