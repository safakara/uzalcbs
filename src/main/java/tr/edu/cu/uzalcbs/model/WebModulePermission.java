package tr.edu.cu.uzalcbs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="WEB_MODULE_PERMISSION")
public class WebModulePermission {
	
	@Id
	@Column(name="ID", nullable=false)
	private Integer id;
	
	@Column(name="ROLE_ID", nullable=false)
	private Integer roleId;
	
	@Column(name="MODULE_ID", nullable=false)
	private Integer moduleId;
	
	@Type(type="boolean")
	@Column(name="OKUMA")
	private Boolean okuma;
	
	@Type(type="boolean")
	@Column(name="YAZMA")
	private Boolean yazma;
	
	@Type(type="boolean")
	@Column(name="SILME")
	private Boolean silme;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Boolean getOkuma() {
		return okuma;
	}

	public void setOkuma(Boolean okuma) {
		this.okuma = okuma;
	}

	public Boolean getYazma() {
		return yazma;
	}

	public void setYazma(Boolean yazma) {
		this.yazma = yazma;
	}

	public Boolean getSilme() {
		return silme;
	}

	public void setSilme(Boolean silme) {
		this.silme = silme;
	}

}
