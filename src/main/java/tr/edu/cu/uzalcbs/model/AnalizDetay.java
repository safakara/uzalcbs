package tr.edu.cu.uzalcbs.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * <p>
 * Pojo mapping TABLE analiz_detay
 * </p>
 * 
 */
@Entity
@Table(name = "analiz_detay")
@SuppressWarnings("serial")
public class AnalizDetay implements Serializable {

	/**
	 * Attribute id.
	 */
	private Integer id;

	/**
	 * Attribute istasyon
	 */
	private Istasyon istasyon;

	/**
	 * Attribute analizTipi
	 */
	private AnalizTipi analizTipi;

	/**
	 * Attribute suMaxSeviye.
	 */
	private Double suMaxSeviye;

	/**
	 * Attribute suMinSeviye.
	 */
	private Double suMinSeviye;

	/**
	 * Attribute suMaxSicaklik.
	 */
	private Double suMaxSicaklik;

	/**
	 * Attribute suMinSicaklik.
	 */
	private Double suMinSicaklik;

	/**
	 * Attribute suMaxTahliye.
	 */
	private Double suMaxTahliye;

	/**
	 * Attribute suMinTahliye.
	 */
	private Double suMinTahliye;

	/**
	 * Attribute numuneDusey.
	 */
	private Double numuneDusey;

	/**
	 * Attribute suDerinligi.
	 */
	private Double suDerinligi;

	/**
	 * Attribute eselSeviye.
	 */
	private Double eselSeviye;

	/**
	 * Attribute olcekSeriNo.
	 */
	private String olcekSeriNo;

	/**
	 * Attribute sedimentYuzdesi.
	 */
	private Double sedimentYuzdesi;

	/**
	 * Attribute sedimentMiktari.
	 */
	private Double sedimentMiktari;

	/**
	 * Attribute kil.
	 */
	private Double kil;

	/**
	 * Attribute silt.
	 */
	private Double silt;

	/**
	 * Attribute kum.
	 */
	private Double kum;

	/**
	 * Attribute tekstur.
	 */
	private Double tekstur;

	/**
	 * Attribute ph.
	 */
	private Double ph;

	/**
	 * Attribute bod.
	 */
	private Double bod;

	/**
	 * Attribute kimyasalMadde
	 */
	private KimyasalMadde kimyasalMadde;

	/**
	 * Attribute kimyasalMaddeDeger.
	 */
	private Double kimyasalMaddeDeger;

	/**
	 * Attribute okumaZamani.
	 */
	private Timestamp okumaZamani;

	/**
	 * Attribute olusturmaTarihi.
	 */
	private Timestamp olusturmaTarihi;

	/**
	 * <p>
	 * </p>
	 * 
	 * @return id
	 */
	@Basic
	@Id
	@GeneratedValue
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            new value for id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * get istasyon
	 */
	@ManyToOne
	@JoinColumn(name = "istasyon_id")
	public Istasyon getIstasyon() {
		return this.istasyon;
	}

	/**
	 * set istasyon
	 */
	public void setIstasyon(Istasyon istasyon) {
		this.istasyon = istasyon;
	}

	/**
	 * get analizTipi
	 */
	@ManyToOne
	@JoinColumn(name = "analiz_tipi_id")
	public AnalizTipi getAnalizTipi() {
		return this.analizTipi;
	}

	/**
	 * set analizTipi
	 */
	public void setAnalizTipi(AnalizTipi analizTipi) {
		this.analizTipi = analizTipi;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return suMaxSeviye
	 */
	@Basic
	@Column(name = "su_max_seviye")
	public Double getSuMaxSeviye() {
		return suMaxSeviye;
	}

	/**
	 * @param suMaxSeviye
	 *            new value for suMaxSeviye
	 */
	public void setSuMaxSeviye(Double suMaxSeviye) {
		this.suMaxSeviye = suMaxSeviye;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return suMinSeviye
	 */
	@Basic
	@Column(name = "su_min_seviye")
	public Double getSuMinSeviye() {
		return suMinSeviye;
	}

	/**
	 * @param suMinSeviye
	 *            new value for suMinSeviye
	 */
	public void setSuMinSeviye(Double suMinSeviye) {
		this.suMinSeviye = suMinSeviye;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return suMaxSicaklik
	 */
	@Basic
	@Column(name = "su_max_sicaklik")
	public Double getSuMaxSicaklik() {
		return suMaxSicaklik;
	}

	/**
	 * @param suMaxSicaklik
	 *            new value for suMaxSicaklik
	 */
	public void setSuMaxSicaklik(Double suMaxSicaklik) {
		this.suMaxSicaklik = suMaxSicaklik;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return suMinSicaklik
	 */
	@Basic
	@Column(name = "su_min_sicaklik")
	public Double getSuMinSicaklik() {
		return suMinSicaklik;
	}

	/**
	 * @param suMinSicaklik
	 *            new value for suMinSicaklik
	 */
	public void setSuMinSicaklik(Double suMinSicaklik) {
		this.suMinSicaklik = suMinSicaklik;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return suMaxTahliye
	 */
	@Basic
	@Column(name = "su_max_tahliye")
	public Double getSuMaxTahliye() {
		return suMaxTahliye;
	}

	/**
	 * @param suMaxTahliye
	 *            new value for suMaxTahliye
	 */
	public void setSuMaxTahliye(Double suMaxTahliye) {
		this.suMaxTahliye = suMaxTahliye;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return suMinTahliye
	 */
	@Basic
	@Column(name = "su_min_tahliye")
	public Double getSuMinTahliye() {
		return suMinTahliye;
	}

	/**
	 * @param suMinTahliye
	 *            new value for suMinTahliye
	 */
	public void setSuMinTahliye(Double suMinTahliye) {
		this.suMinTahliye = suMinTahliye;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return numuneDusey
	 */
	@Basic
	@Column(name = "numune_dusey")
	public Double getNumuneDusey() {
		return numuneDusey;
	}

	/**
	 * @param numuneDusey
	 *            new value for numuneDusey
	 */
	public void setNumuneDusey(Double numuneDusey) {
		this.numuneDusey = numuneDusey;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return suDerinligi
	 */
	@Basic
	@Column(name = "su_derinligi")
	public Double getSuDerinligi() {
		return suDerinligi;
	}

	/**
	 * @param suDerinligi
	 *            new value for suDerinligi
	 */
	public void setSuDerinligi(Double suDerinligi) {
		this.suDerinligi = suDerinligi;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return eselSeviye
	 */
	@Basic
	@Column(name = "esel_seviye")
	public Double getEselSeviye() {
		return eselSeviye;
	}

	/**
	 * @param eselSeviye
	 *            new value for eselSeviye
	 */
	public void setEselSeviye(Double eselSeviye) {
		this.eselSeviye = eselSeviye;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return olcekSeriNo
	 */
	@Basic
	@Column(name = "olcek_seri_no", length = 20)
	public String getOlcekSeriNo() {
		return olcekSeriNo;
	}

	/**
	 * @param olcekSeriNo
	 *            new value for olcekSeriNo
	 */
	public void setOlcekSeriNo(String olcekSeriNo) {
		this.olcekSeriNo = olcekSeriNo;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return sedimentYuzdesi
	 */
	@Basic
	@Column(name = "sediment_yuzdesi")
	public Double getSedimentYuzdesi() {
		return sedimentYuzdesi;
	}

	/**
	 * @param sedimentYuzdesi
	 *            new value for sedimentYuzdesi
	 */
	public void setSedimentYuzdesi(Double sedimentYuzdesi) {
		this.sedimentYuzdesi = sedimentYuzdesi;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return sedimentMiktari
	 */
	@Basic
	@Column(name = "sediment_miktari")
	public Double getSedimentMiktari() {
		return sedimentMiktari;
	}

	/**
	 * @param sedimentMiktari
	 *            new value for sedimentMiktari
	 */
	public void setSedimentMiktari(Double sedimentMiktari) {
		this.sedimentMiktari = sedimentMiktari;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return kil
	 */
	@Basic
	@Column(name = "kil")
	public Double getKil() {
		return kil;
	}

	/**
	 * @param kil
	 *            new value for kil
	 */
	public void setKil(Double kil) {
		this.kil = kil;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return silt
	 */
	@Basic
	@Column(name = "silt")
	public Double getSilt() {
		return silt;
	}

	/**
	 * @param silt
	 *            new value for silt
	 */
	public void setSilt(Double silt) {
		this.silt = silt;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return kum
	 */
	@Basic
	@Column(name = "kum")
	public Double getKum() {
		return kum;
	}

	/**
	 * @param kum
	 *            new value for kum
	 */
	public void setKum(Double kum) {
		this.kum = kum;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return tekstur
	 */
	@Basic
	@Column(name = "tekstur")
	public Double getTekstur() {
		return tekstur;
	}

	/**
	 * @param tekstur
	 *            new value for tekstur
	 */
	public void setTekstur(Double tekstur) {
		this.tekstur = tekstur;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return ph
	 */
	@Basic
	@Column(name = "ph")
	public Double getPh() {
		return ph;
	}

	/**
	 * @param ph
	 *            new value for ph
	 */
	public void setPh(Double ph) {
		this.ph = ph;
	}

	/**
	 * <p>
	 * ��z�nm�� oksijen miktar�
	 * </p>
	 * 
	 * @return bod
	 */
	@Basic
	@Column(name = "bod")
	public Double getBod() {
		return bod;
	}

	/**
	 * @param bod
	 *            new value for bod
	 */
	public void setBod(Double bod) {
		this.bod = bod;
	}

	/**
	 * get kimyasalMadde
	 */
	@ManyToOne
	@JoinColumn(name = "kimyasal_madde_id")
	public KimyasalMadde getKimyasalMadde() {
		return this.kimyasalMadde;
	}

	/**
	 * set kimyasalMadde
	 */
	public void setKimyasalMadde(KimyasalMadde kimyasalMadde) {
		this.kimyasalMadde = kimyasalMadde;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return kimyasalMaddeDeger
	 */
	@Basic
	@Column(name = "kimyasal_madde_deger")
	public Double getKimyasalMaddeDeger() {
		return kimyasalMaddeDeger;
	}

	/**
	 * @param kimyasalMaddeDeger
	 *            new value for kimyasalMaddeDeger
	 */
	public void setKimyasalMaddeDeger(Double kimyasalMaddeDeger) {
		this.kimyasalMaddeDeger = kimyasalMaddeDeger;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return okumaZamani
	 */
	@Basic
	@Column(name = "okuma_zamani")
	public Timestamp getOkumaZamani() {
		return okumaZamani;
	}

	/**
	 * @param okumaZamani
	 *            new value for okumaZamani
	 */
	public void setOkumaZamani(Timestamp okumaZamani) {
		this.okumaZamani = okumaZamani;
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @return olusturmaTarihi
	 */
	@Basic
	@Column(name = "olusturma_tarihi")
	public Timestamp getOlusturmaTarihi() {
		return olusturmaTarihi;
	}

	/**
	 * @param olusturmaTarihi
	 *            new value for olusturmaTarihi
	 */
	public void setOlusturmaTarihi(Timestamp olusturmaTarihi) {
		this.olusturmaTarihi = olusturmaTarihi;
	}

}