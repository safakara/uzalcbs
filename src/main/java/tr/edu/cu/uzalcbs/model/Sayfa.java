package tr.edu.cu.uzalcbs.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p>Pojo mapping TABLE sayfa</p>
 * 
 */
@Entity
@Table(name = "sayfa")
@SuppressWarnings("serial")
public class Sayfa implements Serializable {

	/**
	 * Attribute id.
	 */
	private Integer id;
	
	/**
	 * Attribute adi.
	 */
	private String adi;
	
	/**
	 * Attribute url.
	 */
	private String url;
	
	/**
	 * Attribute olusturmaTarihi.
	 */
	private Timestamp olusturmaTarihi;
	
	/**
	 * List of SayfaYetkileri
	 */
	private List<SayfaYetkileri> sayfaYetkileris = null;

	
	/**
	 * <p> 
	 * </p>
	 * @return id
	 */
	@Basic
	@Id
	@GeneratedValue
	@Column(name = "id")
		public Integer getId() {
		return id;
	}

	/**
	 * @param id new value for id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return adi
	 */
	@Basic
	@Column(name = "adi", length = 100)
		public String getAdi() {
		return adi;
	}

	/**
	 * @param adi new value for adi 
	 */
	public void setAdi(String adi) {
		this.adi = adi;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return url
	 */
	@Basic
	@Column(name = "url", length = 100)
		public String getUrl() {
		return url;
	}

	/**
	 * @param url new value for url 
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return olusturmaTarihi
	 */
	@Basic
	@Column(name = "olusturma_tarihi")
		public Timestamp getOlusturmaTarihi() {
		return olusturmaTarihi;
	}

	/**
	 * @param olusturmaTarihi new value for olusturmaTarihi 
	 */
	public void setOlusturmaTarihi(Timestamp olusturmaTarihi) {
		this.olusturmaTarihi = olusturmaTarihi;
	}
	
	/**
	 * Get the list of SayfaYetkileri
	 */
	 @OneToMany(mappedBy="sayfa")
	 public List<SayfaYetkileri> getSayfaYetkileris() {
	 	return this.sayfaYetkileris;
	 }
	 
	/**
	 * Set the list of SayfaYetkileri
	 */
	 public void setSayfaYetkileris(List<SayfaYetkileri> sayfaYetkileris) {
	 	this.sayfaYetkileris = sayfaYetkileris;
	 }


}