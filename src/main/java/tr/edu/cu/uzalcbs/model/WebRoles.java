package tr.edu.cu.uzalcbs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="WEB_ROLES", uniqueConstraints=@UniqueConstraint(columnNames = {"ID","NAME"}))
public class WebRoles {
	
	@Id
	@Column(name="ID", nullable=false)
	private Integer id;
	
	@Column(name="NAME", nullable=false, length=50)
	private String name;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
