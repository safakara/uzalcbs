package tr.edu.cu.uzalcbs.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p>Pojo mapping TABLE kimyasal_madde</p>
 */
@Entity
@Table(name = "kimyasal_madde")
@SuppressWarnings("serial")
public class KimyasalMadde implements Serializable {

	/**
	 * Attribute id.
	 */
	private Integer id;
	
	/**
	 * Attribute isim.
	 */
	private String isim;
	
	/**
	 * Attribute olusturmaTarihi.
	 */
	private Timestamp olusturmaTarihi;
	
	/**
	 * List of AnalizDetay
	 */
	private List<AnalizDetay> analizDetays = null;

	
	/**
	 * <p> 
	 * </p>
	 * @return id
	 */
	@Basic
	@Id
	@GeneratedValue
	@Column(name = "id")
		public Integer getId() {
		return id;
	}

	/**
	 * @param id new value for id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return isim
	 */
	@Basic
	@Column(name = "isim", length = 50)
		public String getIsim() {
		return isim;
	}

	/**
	 * @param isim new value for isim 
	 */
	public void setIsim(String isim) {
		this.isim = isim;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return olusturmaTarihi
	 */
	@Basic
	@Column(name = "olusturma_tarihi")
		public Timestamp getOlusturmaTarihi() {
		return olusturmaTarihi;
	}

	/**
	 * @param olusturmaTarihi new value for olusturmaTarihi 
	 */
	public void setOlusturmaTarihi(Timestamp olusturmaTarihi) {
		this.olusturmaTarihi = olusturmaTarihi;
	}
	
	/**
	 * Get the list of AnalizDetay
	 */
	 @OneToMany(mappedBy="kimyasalMadde")
	 public List<AnalizDetay> getAnalizDetays() {
	 	return this.analizDetays;
	 }
	 
	/**
	 * Set the list of AnalizDetay
	 */
	 public void setAnalizDetays(List<AnalizDetay> analizDetays) {
	 	this.analizDetays = analizDetays;
	 }


}