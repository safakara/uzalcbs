package tr.edu.cu.uzalcbs.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * <p>Pojo mapping TABLE kullanici</p>
 */
@Entity
@Table(name = "kullanici", uniqueConstraints=@UniqueConstraint(columnNames = {"mail"}))
@SuppressWarnings("serial")
public class Kullanici implements Serializable {

	/**
	 * Attribute id.
	 */
	private Integer id;
	
	/**
	 * Attribute kullaniciGrup
	 */
	 private KullaniciGrup kullaniciGrup;	

	/**
	 * Attribute mail.
	 */
	private String mail;
	
	/**
	 * Attribute sifre.
	 */
	private String sifre;
	
	/**
	 * Attribute isim.
	 */
	private String isim;
	
	/**
	 * Attribute telefon.
	 */
	private String telefon;
	
	/**
	 * Attribute adres.
	 */
	private String adres;
	
	/**
	 * Attribute olusturmaTarihi.
	 */
	private Timestamp olusturmaTarihi;
	
	
	/**
	 * <p> 
	 * </p>
	 * @return id
	 */
	@Basic
	@Id
	@GeneratedValue
	@Column(name = "id")
		public Integer getId() {
		return id;
	}

	/**
	 * @param id new value for id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * get kullaniciGrup
	 */
	@ManyToOne
	@JoinColumn(name = "kullanici_grup_id")
	public KullaniciGrup getKullaniciGrup() {
		return this.kullaniciGrup;
	}
	
	/**
	 * set kullaniciGrup
	 */
	public void setKullaniciGrup(KullaniciGrup kullaniciGrup) {
		this.kullaniciGrup = kullaniciGrup;
	}

	/**
	 * <p> 
	 * </p>
	 * @return mail
	 */
	@Basic
	@Column(name = "mail", length = 50)
		public String getMail() {
		return mail;
	}

	/**
	 * @param mail new value for mail 
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return sifre
	 */
	@Basic
	@Column(name = "sifre", length = 32)
		public String getSifre() {
		return sifre;
	}

	/**
	 * @param sifre new value for sifre 
	 */
	public void setSifre(String sifre) {
		this.sifre = sifre;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return isim
	 */
	@Basic
	@Column(name = "isim", length = 50)
		public String getIsim() {
		return isim;
	}

	/**
	 * @param isim new value for isim 
	 */
	public void setIsim(String isim) {
		this.isim = isim;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return telefon
	 */
	@Basic
	@Column(name = "telefon", length = 20)
		public String getTelefon() {
		return telefon;
	}

	/**
	 * @param telefon new value for telefon 
	 */
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return adres
	 */
	@Basic
	@Column(name = "adres", length = 100)
		public String getAdres() {
		return adres;
	}

	/**
	 * @param adres new value for adres 
	 */
	public void setAdres(String adres) {
		this.adres = adres;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return olusturmaTarihi
	 */
	@Basic
	@Column(name = "olusturma_tarihi")
		public Timestamp getOlusturmaTarihi() {
		return olusturmaTarihi;
	}

	/**
	 * @param olusturmaTarihi new value for olusturmaTarihi 
	 */
	public void setOlusturmaTarihi(Timestamp olusturmaTarihi) {
		this.olusturmaTarihi = olusturmaTarihi;
	}
	


}