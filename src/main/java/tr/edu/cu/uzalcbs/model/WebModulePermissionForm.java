package tr.edu.cu.uzalcbs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

@Entity
public class WebModulePermissionForm {
	
	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name="MODULE_ID")
	private Integer moduleId;
	
	@Column(name="ROLE_ID")
	private Integer roleId;
	
	@Column(name="MODULE_ADI")
	private String moduleAdi;
	
	@Column(name="MODULE_ON_EK")
	private String moduleOnEk;
	
	@Column(name="ROLE_ADI")
	private String roleAdi;
	
	@Type(type="boolean")
	@Column(name="OKUMA")
	private Boolean okuma;
	
	@Type(type="boolean")
	@Column(name="YAZMA")
	private Boolean yazma;
	
	@Type(type="boolean")
	@Column(name="SILME")
	private Boolean silme;
	
	@Column(name="KONTROL")
	private Integer kontrol;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleAdi() {
		return moduleAdi;
	}

	public String getModuleOnEk() {
		return moduleOnEk;
	}

	public void setModuleOnEk(String moduleOnEk) {
		this.moduleOnEk = moduleOnEk;
	}

	public void setModuleAdi(String moduleAdi) {
		this.moduleAdi = moduleAdi;
	}

	public String getRoleAdi() {
		return roleAdi;
	}

	public void setRoleAdi(String roleAdi) {
		this.roleAdi = roleAdi;
	}

	public Boolean getOkuma() {
		return okuma;
	}

	public void setOkuma(Boolean okuma) {
		this.okuma = okuma;
	}

	public Boolean getYazma() {
		return yazma;
	}

	public void setYazma(Boolean yazma) {
		this.yazma = yazma;
	}

	public Boolean getSilme() {
		return silme;
	}

	public void setSilme(Boolean silme) {
		this.silme = silme;
	}

	public Integer getKontrol() {
		return kontrol;
	}

	public void setKontrol(Integer kontrol) {
		this.kontrol = kontrol;
	}

}

