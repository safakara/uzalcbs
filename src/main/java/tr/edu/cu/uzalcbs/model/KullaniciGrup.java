package tr.edu.cu.uzalcbs.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p>Pojo mapping TABLE kullanici_grup</p>
 * 
 */
@Entity
@Table(name = "kullanici_grup")
@SuppressWarnings("serial")
public class KullaniciGrup implements Serializable {

	/**
	 * Attribute id.
	 */
	private Integer id;
	
	/**
	 * Attribute isim.
	 */
	private String isim;
	
	/**
	 * Attribute olusturmaTarihi.
	 */
	private Timestamp olusturmaTarihi;
	
	/**
	 * List of Kullanici
	 */
	private List<Kullanici> kullanicis = null;

	/**
	 * List of SayfaYetkileri
	 */
	private List<SayfaYetkileri> sayfaYetkileris = null;

	
	/**
	 * <p> 
	 * </p>
	 * @return id
	 */
	@Basic
	@Id
	@GeneratedValue
	@Column(name = "id")
		public Integer getId() {
		return id;
	}

	/**
	 * @param id new value for id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return isim
	 */
	@Basic
	@Column(name = "isim", length = 50)
		public String getIsim() {
		return isim;
	}

	/**
	 * @param isim new value for isim 
	 */
	public void setIsim(String isim) {
		this.isim = isim;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return olusturmaTarihi
	 */
	@Basic
	@Column(name = "olusturma_tarihi")
		public Timestamp getOlusturmaTarihi() {
		return olusturmaTarihi;
	}

	/**
	 * @param olusturmaTarihi new value for olusturmaTarihi 
	 */
	public void setOlusturmaTarihi(Timestamp olusturmaTarihi) {
		this.olusturmaTarihi = olusturmaTarihi;
	}
	
	/**
	 * Get the list of Kullanici
	 */
	 @OneToMany(mappedBy="kullaniciGrup")
	 public List<Kullanici> getKullanicis() {
	 	return this.kullanicis;
	 }
	 
	/**
	 * Set the list of Kullanici
	 */
	 public void setKullanicis(List<Kullanici> kullanicis) {
	 	this.kullanicis = kullanicis;
	 }
	/**
	 * Get the list of SayfaYetkileri
	 */
	 @OneToMany(mappedBy="kullaniciGrup")
	 public List<SayfaYetkileri> getSayfaYetkileris() {
	 	return this.sayfaYetkileris;
	 }
	 
	/**
	 * Set the list of SayfaYetkileri
	 */
	 public void setSayfaYetkileris(List<SayfaYetkileri> sayfaYetkileris) {
	 	this.sayfaYetkileris = sayfaYetkileris;
	 }


}