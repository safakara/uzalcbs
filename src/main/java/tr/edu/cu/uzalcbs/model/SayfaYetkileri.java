package tr.edu.cu.uzalcbs.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * <p>Pojo mapping TABLE sayfa_yetkileri</p>
 * 
 */
@Entity
@Table(name = "sayfa_yetkileri")
@SuppressWarnings("serial")
public class SayfaYetkileri implements Serializable {

	/**
	 * Attribute id.
	 */
	private Integer id;
	
	/**
	 * Attribute sayfa
	 */
	 private Sayfa sayfa;	

	/**
	 * Attribute kullaniciGrup
	 */
	 private KullaniciGrup kullaniciGrup;	

	/**
	 * Attribute yetkiTipi
	 */
	 private YetkiTipi yetkiTipi;	

	/**
	 * Attribute olusturmaTarihi.
	 */
	private Timestamp olusturmaTarihi;
	
	
	/**
	 * <p> 
	 * </p>
	 * @return id
	 */
	@Basic
	@Id
	@GeneratedValue
	@Column(name = "id")
		public Integer getId() {
		return id;
	}

	/**
	 * @param id new value for id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * get sayfa
	 */
	@ManyToOne
	@JoinColumn(name = "sayfa_id")
	public Sayfa getSayfa() {
		return this.sayfa;
	}
	
	/**
	 * set sayfa
	 */
	public void setSayfa(Sayfa sayfa) {
		this.sayfa = sayfa;
	}

	/**
	 * get kullaniciGrup
	 */
	@ManyToOne
	@JoinColumn(name = "kullanici_grup_id")
	public KullaniciGrup getKullaniciGrup() {
		return this.kullaniciGrup;
	}
	
	/**
	 * set kullaniciGrup
	 */
	public void setKullaniciGrup(KullaniciGrup kullaniciGrup) {
		this.kullaniciGrup = kullaniciGrup;
	}

	/**
	 * get yetkiTipi
	 */
	@ManyToOne
	@JoinColumn(name = "yetki_tipi_id")
	public YetkiTipi getYetkiTipi() {
		return this.yetkiTipi;
	}
	
	/**
	 * set yetkiTipi
	 */
	public void setYetkiTipi(YetkiTipi yetkiTipi) {
		this.yetkiTipi = yetkiTipi;
	}

	/**
	 * <p> 
	 * </p>
	 * @return olusturmaTarihi
	 */
	@Basic
	@Column(name = "olusturma_tarihi")
		public Timestamp getOlusturmaTarihi() {
		return olusturmaTarihi;
	}

	/**
	 * @param olusturmaTarihi new value for olusturmaTarihi 
	 */
	public void setOlusturmaTarihi(Timestamp olusturmaTarihi) {
		this.olusturmaTarihi = olusturmaTarihi;
	}
	


}