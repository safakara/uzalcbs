package tr.edu.cu.uzalcbs.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p>Pojo mapping TABLE istasyon</p>
 */
@Entity
@Table(name = "istasyon")
@SuppressWarnings("serial")
public class Istasyon implements Serializable {

	/**
	 * Attribute id.
	 */
	private Integer id;
	
	/**
	 * Attribute isim.
	 */
	private String isim;
	
	/**
	 * Attribute aygitTipi.
	 */
	private String aygitTipi;
	
	/**
	 * Attribute ip.
	 */
	private String ip;
	
	/**
	 * Attribute port.
	 */
	private Integer port;
	
	/**
	 * Attribute bolge.
	 */
	private Boolean bolge;
	
	/**
	 * Attribute enlem.
	 */
	private Double enlem;
	
	/**
	 * Attribute boylam.
	 */
	private Double boylam;
	
	/**
	 * Attribute olusturmaTarihi.
	 */
	private Timestamp olusturmaTarihi;
	
	/**
	 * List of AnalizDetay
	 */
	private List<AnalizDetay> analizDetays = null;

	
	/**
	 * <p> 
	 * </p>
	 * @return id
	 */
	@Basic
	@Id
	@GeneratedValue
	@Column(name = "id")
		public Integer getId() {
		return id;
	}

	/**
	 * @param id new value for id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return isim
	 */
	@Basic
	@Column(name = "isim", length = 100)
		public String getIsim() {
		return isim;
	}

	/**
	 * @param isim new value for isim 
	 */
	public void setIsim(String isim) {
		this.isim = isim;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return aygitTipi
	 */
	@Basic
	@Column(name = "aygit_tipi", length = 15)
		public String getAygitTipi() {
		return aygitTipi;
	}

	/**
	 * @param aygitTipi new value for aygitTipi 
	 */
	public void setAygitTipi(String aygitTipi) {
		this.aygitTipi = aygitTipi;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return ip
	 */
	@Basic
	@Column(name = "ip", length = 15)
		public String getIp() {
		return ip;
	}

	/**
	 * @param ip new value for ip 
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return port
	 */
	@Basic
	@Column(name = "port")
		public Integer getPort() {
		return port;
	}

	/**
	 * @param port new value for port 
	 */
	public void setPort(Integer port) {
		this.port = port;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return bolge
	 */
	@Basic
	@Column(name = "bolge")
		public Boolean getBolge() {
		return bolge;
	}

	/**
	 * @param bolge new value for bolge 
	 */
	public void setBolge(Boolean bolge) {
		this.bolge = bolge;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return enlem
	 */
	@Basic
	@Column(name = "enlem")
		public Double getEnlem() {
		return enlem;
	}

	/**
	 * @param enlem new value for enlem 
	 */
	public void setEnlem(Double enlem) {
		this.enlem = enlem;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return boylam
	 */
	@Basic
	@Column(name = "boylam")
		public Double getBoylam() {
		return boylam;
	}

	/**
	 * @param boylam new value for boylam 
	 */
	public void setBoylam(Double boylam) {
		this.boylam = boylam;
	}
	
	/**
	 * <p> 
	 * </p>
	 * @return olusturmaTarihi
	 */
	@Basic
	@Column(name = "olusturma_tarihi")
		public Timestamp getOlusturmaTarihi() {
		return olusturmaTarihi;
	}

	/**
	 * @param olusturmaTarihi new value for olusturmaTarihi 
	 */
	public void setOlusturmaTarihi(Timestamp olusturmaTarihi) {
		this.olusturmaTarihi = olusturmaTarihi;
	}
	
	/**
	 * Get the list of AnalizDetay
	 */
	 @OneToMany(mappedBy="istasyon")
	 public List<AnalizDetay> getAnalizDetays() {
	 	return this.analizDetays;
	 }
	 
	/**
	 * Set the list of AnalizDetay
	 */
	 public void setAnalizDetays(List<AnalizDetay> analizDetays) {
	 	this.analizDetays = analizDetays;
	 }


}