package tr.edu.cu.uzalcbs.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "dinamik_rapor")
public class DinamikRapor {
	
	@Basic
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name = "aciklama", nullable = true, length=500)
	private String aciklama;
	
	@Column(name = "sql_query", nullable = false)
	private String sqlQuery;
	
	@Column(name = "form_inputs")
	private String formInputs;
	
	@Column(name = "js")
	private String js;
	
	@Type(type="boolean")
	@Column(name = "query_auto_exec", length = 1)
	private Boolean queryAutoExec;
	
	@Type(type="boolean")
	@Column(name = "js_paging", length = 1)
	private Boolean jsPaging;
	
	@Column(name = "js_page_item_count")
	private Integer jsPageItemCount;
	
	@Type(type="boolean")
	@Column(name = "durum", length = 1)
	private Boolean durum;
	
	@Column(name = "yetki", length = 100)
	private String yetki;
	
	@Column(name = "kullanici", length = 40)
	private String kullanici;
	
	@DateTimeFormat(pattern="dd.MM.yyyy hh:mm")
	@Column(name="tarih", nullable = false)
	private Date tarih;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getSqlQuery() {
		return sqlQuery;
	}

	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}

	public String getFormInputs() {
		return formInputs;
	}

	public void setFormInputs(String formInputs) {
		this.formInputs = formInputs;
	}

	public String getJs() {
		return js;
	}

	public void setJs(String js) {
		this.js = js;
	}

	public Boolean getQueryAutoExec() {
		return queryAutoExec;
	}

	public void setQueryAutoExec(Boolean queryAutoExec) {
		this.queryAutoExec = queryAutoExec;
	}

	public Boolean getJsPaging() {
		return jsPaging;
	}

	public void setJsPaging(Boolean jsPaging) {
		this.jsPaging = jsPaging;
	}

	public Integer getJsPageItemCount() {
		return jsPageItemCount;
	}

	public void setJsPageItemCount(Integer jsPageItemCount) {
		this.jsPageItemCount = jsPageItemCount;
	}

	public Boolean getDurum() {
		return durum;
	}

	public void setDurum(Boolean durum) {
		this.durum = durum;
	}

	public String getYetki() {
		return yetki;
	}

	public void setYetki(String yetki) {
		this.yetki = yetki;
	}

	public String getKullanici() {
		return kullanici;
	}

	public void setKullanici(String kullanici) {
		this.kullanici = kullanici;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

}
