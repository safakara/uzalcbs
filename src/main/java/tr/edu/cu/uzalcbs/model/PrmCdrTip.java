package tr.edu.cu.uzalcbs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "prm_cdr_tip")
public class PrmCdrTip {
	
	@Id
	@Column(name = "tip", unique = true, nullable = false)
	private Integer tip;
	
	@Column(name = "aciklama", nullable = false, length = 30)
	private String aciklama;
	
	@Column(name="hizmet_turu", nullable = false)
	private Integer hizmetTuru;

	public Integer getTip() {
		return tip;
	}

	public void setTip(Integer tip) {
		this.tip = tip;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getHizmetTuru() {
		return hizmetTuru;
	}

	public void setHizmetTuru(Integer hizmetTuru) {
		this.hizmetTuru = hizmetTuru;
	}

	
}
