package tr.edu.cu.uzalcbs.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import tr.edu.cu.uzalcbs.model.Kullanici;
import tr.edu.cu.uzalcbs.service.KullaniciService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
 
	@Autowired
	KullaniciService kullaniciService;
	
	@Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        
    	Authentication auth = null;
    	Kullanici kullanici = new Kullanici();
    	kullanici.setMail(authentication.getPrincipal().toString());
    	kullanici.setSifre(DigestUtils.md5Hex(authentication.getCredentials().toString()));
    	kullanici = kullaniciService.getKullaniciFindByCriteria(kullanici);
		
    	if(kullanici != null) {
    		List<GrantedAuthority> grantedAuths = new ArrayList<>();
    		grantedAuths.add(new SimpleGrantedAuthority("ROLE_UZALCBS_USER"));
    		grantedAuths.add(new SimpleGrantedAuthority("ROLE_" + kullanici.getKullaniciGrup().getIsim()));
    		
		    List<String> yetkiListesi = kullaniciService.getKullaniciYetkiList(kullanici);
            
            for (String tmp : yetkiListesi) {
            	grantedAuths.add(new SimpleGrantedAuthority(tmp));
			}
            auth = new UsernamePasswordAuthenticationToken(kullanici.getMail(), kullanici.getSifre(), grantedAuths);
		    
    	}
		 
    	return auth;
       
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}