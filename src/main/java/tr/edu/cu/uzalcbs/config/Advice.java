package tr.edu.cu.uzalcbs.config;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import tr.edu.cu.uzalcbs.model.DinamikRapor;
import tr.edu.cu.uzalcbs.model.Kullanici;
import tr.edu.cu.uzalcbs.service.DinamikRaporService;
import tr.edu.cu.uzalcbs.service.KullaniciService;

@ControllerAdvice
public class Advice {
	
	@Autowired
	KullaniciService kullaniciService;
	
	@Autowired
	private DinamikRaporService dinamikRaporService;
	
	
	@ModelAttribute
	public void addAttrToModel(HttpServletRequest request) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth !=null && request.getSession().getAttribute("kullaniciAdi") == null ){
			List<DinamikRapor> dinamikRaporList = null;
			String kullaniciAdi = null;
			
			Kullanici kullanici = kullaniciService.getKullaniciFindByMail(auth.getPrincipal().toString());
			
			if (kullanici != null) {
				
				kullaniciAdi = kullanici.getIsim();
				dinamikRaporList = kullanici.getKullaniciGrup().getId() == 1 ? 
						dinamikRaporService.getAktifDinamikRaporList() : 
						dinamikRaporService.getAktifDinamikRaporListFindByKullaniciGrup(kullanici.getKullaniciGrup().getId());
			
			}
	    	
			request.getSession().setAttribute("kullaniciAdi", kullaniciAdi);
			request.getSession().setAttribute("dinamikRaporList", dinamikRaporList);
		}
		
	}
}