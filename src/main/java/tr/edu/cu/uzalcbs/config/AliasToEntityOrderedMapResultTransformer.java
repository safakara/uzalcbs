package tr.edu.cu.uzalcbs.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.hibernate.transform.AliasedTupleSubsetResultTransformer;

public class AliasToEntityOrderedMapResultTransformer extends AliasedTupleSubsetResultTransformer {

	public static final AliasToEntityOrderedMapResultTransformer INSTANCE = new AliasToEntityOrderedMapResultTransformer();

	private AliasToEntityOrderedMapResultTransformer() {
	}

	public Object transformTuple(Object[] tuple, String[] aliases) {

		Map result = new LinkedHashMap(tuple.length);
		for (int i = 0; i < tuple.length; i++) {
			String alias = aliases[i];
			if (alias != null) {
				result.put(alias, tuple[i]);
			}
		}
		
		return result;
	}

	public boolean isTransformedValueATupleElement(String[] aliases, int tupleLength) {
		return false;
	}

	private Object readResolve() {
		return INSTANCE;
	}
}
