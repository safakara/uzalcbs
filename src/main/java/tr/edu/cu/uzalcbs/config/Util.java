package tr.edu.cu.uzalcbs.config;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.time.FastDateFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import tr.edu.cu.uzalcbs.service.KullaniciService;

@Component
public class Util {
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private KullaniciService kullaniciService;
	
	public static FastDateFormat dateFormat = FastDateFormat.getInstance(
			"dd.MM.yyyy HH:mm", Locale.getDefault());

	public static String checkReportFormat(String format) {
		String result = "pdf";
		if ("html".equalsIgnoreCase(format)) {
			result = "html";
		} else if ("xls".equalsIgnoreCase(format)) {
			result = "xls";
		} else if ("csv".equalsIgnoreCase(format)) {
			result = "csv";
		}

		return result;
	}

	public static String toAuthenticatePrefix(String text) {
		Character[] from = { 'Ğ', 'Ü', 'Ş', 'İ', 'Ö', 'Ç' };
		Character[] to = { 'G', 'U', 'S', 'I', 'O', 'C' };

		text = text.trim().toUpperCase().replaceAll(" +", "_");
		if (from.length == to.length)
			for (int i = 0; i < from.length; i++)
				text = text.replace(from[i], to[i]);

		return text;
	}

	public String getKullaniciAdi() {
		
		String username = null;
		if(request != null){
			
			if(request.getSession().getAttribute("kullaniciAdi") != null) {  // sessionda kullaniciAdi aranıyor
				username = request.getSession().getAttribute("kullaniciAdi").toString();
			} else { // sessionda kullaniciAdi yoksa mail adresi ile kullaniciAdi bulunuyor
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				if (!(auth instanceof AnonymousAuthenticationToken)) {
					username = kullaniciService.getKullaniciFindByMail(auth.getName()).getIsim();
				}
			}
			
		}
		
		return username;
	}

}
